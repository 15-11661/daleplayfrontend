import React from 'react';

const EnvVariables = () => {
  return (
    <div>
      <h1>Environment Variables</h1>
      <p>NEXT_PUBLIC_API_URL: {process.env.NEXT_PUBLIC_API_URL}</p>
      <p>NEXT_PUBLIC_TOKEN: {process.env.NEXT_PUBLIC_TOKEN}</p>
    </div>
  );
};

export default EnvVariables;