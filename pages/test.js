import React, { useEffect, useState } from 'react';
import { GlikHttpService } from '../src/services/glikHttp.service';

const TestApi = () => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  const fetchData = async () => {
    try {
      const apiService = new GlikHttpService();
      const result = await apiService.get({ endpoint: '/test-endpoint' });
      setData(result);
      setError(null);
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    fetchData(); // Realizar la primera llamada al montar el componente

    const intervalId = setInterval(() => {
      fetchData(); // Realizar una llamada cada 60 segundos
    }, 60000);

    return () => {
      clearInterval(intervalId); // Limpiar el intervalo al desmontar el componente
    };
  }, []);

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  if (!data) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>API Data</h1>
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </div>
  );
};

export default TestApi;
