import { UserModel } from '@/models/user.model';
import { AuthService } from '@/services/auth/auth.service';
import { create } from 'zustand';

interface State {
  user: UserModel | undefined;
  token: string | undefined;
  isLoggedIn: boolean;
  authService: AuthService;
}

interface Actions {
  actions: {
    setUser: (value?: UserModel | undefined) => void;
  };
}

/**
 * Required for zustand stores, as the lib doesn't expose this type
 */
export type ExtractState<S> = S extends {
  getState: () => infer T;
}
  ? T
  : never;

export const useAuthStore = create<State & Actions>()((set) => ({
  // State
  user: undefined,
  token: undefined,
  isLoggedIn: false,
  authService: new AuthService(),

  // Actions
  actions: {
    setUser: (value) =>
      set(() => {
        return { user: value, isLoggedIn: !!value?.id, token: value?.token };
      }),
  },
}));

// Selectors
const tokenSelector = (state: ExtractState<typeof useAuthStore>) => state.token;
const actionsSelector = (state: ExtractState<typeof useAuthStore>) =>
  state.actions;

// Getters
export const getToken = () => tokenSelector(useAuthStore.getState());
export const getActions = () => actionsSelector(useAuthStore.getState());

// Export all states in hooks
export const useUser = () => useAuthStore((state) => state.user);
export const useToken = () => useAuthStore((state) => state.token);
export const useIsLoggedIn = () => useAuthStore((state) => state.isLoggedIn);
export const useAuthService = () => useAuthStore((state) => state.authService);

// Export all actions
export const useAuthActions = () => useAuthStore((state) => state.actions);

