export interface CategoryModel {
  id: string;
  name: string;
  parent: string | null;
}
