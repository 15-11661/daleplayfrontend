export interface ProductModel {
  id: number;
  category: number;
  description: string;
  name: string;
  stock: number;
  leasePrice: number;
  initialFee: number;
  cashPrice: number;
  brand: string;
  internalId: string;
  profileImage: string;
  extra: object;
  leaseOptions: { [key: string]: LeaseInfo };
  images: string[];
  createdAt: string;
  isFeatured: boolean;
}

export interface LeaseInfo {
  initialFee: number;
  weeklyFee: number;
  total: number;
}
