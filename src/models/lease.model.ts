//Lease Model
import { leasesStatusEnum } from '@/services/leases/leasesStatus.enum';

import { ProductModel } from './product.model';

export interface LeaseModel {
  id: number;
  status: keyof typeof leasesStatusEnum;
  createdAt: string;
  user: string;
  userScore: number;
  product: number;
  fullProductPrice: number;
  initialFee: number;
  monthlyFee: number;
  feesNumber: number;
  address: string;
  rider: unknown;
  loanGrantor: number;
  leaseReason: string;
  payments: unknown[];
  nextPaymentDate: string;
  type: string;
  country_user_id: string;
}

export interface Company {
  name: string;
  web_page: string;
  instagram: string;
  facebook: string;
  address: string;
}

export interface LoanGrantor {
  first_name: string;
  last_name: string;
  relationship: string;
  phone_number: string;
  email: string;
  address_room: string;
  land_line_number: string;
}

export interface Lease {
  country_user_id: string;
  type: string;
  state: string;
  user_score: string;
  product: string;
  full_product_price: string;
  initial_fee: string;
  monthly_fee: string;
  fees_number: string;
  address: string;
  lease_reason: string;
}

export interface LeaseCreateData {
  lease: Lease;
  company: Company;
  loanGrantor: LoanGrantor;
}
