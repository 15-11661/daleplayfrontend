import * as z from 'zod';

export const UserFormSchema = z.object({
  username: z.string({
    invalid_type_error: 'El nombre del usuario debe ser una cadena de caracteres.',
    required_error: 'Debes ingresar un usuario válido',
  }),
  email: z.string({
    invalid_type_error: 'El correo debe ser una cadena de caracteres.',
    required_error: 'Debes ingresar un correo',
  }),
  firstName: z.string({
    invalid_type_error: 'El nombre debe ser una cadena de caracteres.',
    required_error: 'Debes ingresar un nombre',
  }),
  lastName: z.string({
    invalid_type_error: 'El apellido debe ser una cadena de caracteres.',
    required_error: 'Debes ingresar un apellido',
  }),
  countryUserId: z.string({
    invalid_type_error: 'La ciudad debe ser una cadena de caracteres.',
    required_error: 'Debes ingresar una ciudad',
  }),
  phoneNumber: z.string({
    invalid_type_error: 'El número de teléfono debe ser una cadena de caracteres.',
    required_error: 'Debes ingresar un número de teléfono',
  }),
  password: z
    .string({
      invalid_type_error: 'La contraseña debe ser una cadena de caracteres.',
      required_error: 'Debes ingresar una contraseña',
    })
    .min(6, 'La contraseña debe ser de al menos 6 caracteres'),
});

export type UserFormValues = z.infer<typeof UserFormSchema>;
