import * as z from 'zod';

export const ProductFormSchema = z.object({
  name: z
    .string({
      invalid_type_error:
        'El nombre del producto debe ser una cadena de caracteres.',
      required_error: 'Debes ingresar un nombre válido',
    })
    .min(3, 'El nombre del producto debe ser de al menos 3 caracteres'),
  description: z
    .string({
      invalid_type_error: 'La descripcion debe ser una cadena de caracteres.',
      required_error: 'Debes ingresar una descripcion del producto',
    })
    .min(6, 'La contraseña debe ser de al menos 6 caracteres'),
  category: z
    .string({
      invalid_type_error: 'La categoria debe ser un numero.',
      required_error: 'Debes ingresar una categoria',
    })
    .regex(/^[0-9]+$/, 'La categoria debe contener  solo números'),
  stock: z
    .string({
      invalid_type_error: 'El stock debe ser un numero.',
      required_error: 'Debes ingresar un stock',
    })
    .regex(/^[0-9]+$/, 'El stock debe contener  solo números'),
  leasePrice: z
    .string({
      invalid_type_error: 'El precio de alquiler debe ser un numero.',
      required_error: 'Debes ingresar un precio de alquiler',
    })
    .regex(/^[0-9]+$/, 'El precio de alquiler debe contener  solo números'),
  initialFee: z
    .string({
      invalid_type_error: 'El precio de entrada debe ser un numero.',
      required_error: 'Debes ingresar un precio de entrada',
    })
    .regex(/^[0-9]+$/, 'El precio de entrada debe contener  solo números'),
  cashPrice: z
    .string({
      invalid_type_error: 'El precio de contado debe ser un numero.',
      required_error: 'Debes ingresar un precio de contado',
    })
    .regex(/^[0-9]+$/, 'El precio de contado debe contener  solo números'),
  brand: z.string({
    invalid_type_error: 'La marca debe ser una cadena de caracteres.',
    required_error: 'Debes ingresar una marca',
  }),
  extra: z.object({
    height: z
      .string({
        invalid_type_error: 'La altura debe ser un numero.',
        required_error: 'Debes ingresar una altura',
      })
      .regex(/^[0-9]+$/, 'La altura debe contener  solo números'),
    width: z
      .string({
        invalid_type_error: 'El ancho debe ser un numero.',
        required_error: 'Debes ingresar un ancho',
      })
      .regex(/^[0-9]+$/, 'El ancho debe contener  solo números'),
  }),
});

export type LoginFormData = z.infer<typeof ProductFormSchema>;
