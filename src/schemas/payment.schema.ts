import * as z from 'zod';

export const PaymentFormSchema = z.object({
  amount: z.string({
    invalid_type_error: 'El monto debe ser un numero.',
    required_error: 'Debes ingresar el monto del pago',
  }),
  currency: z.string({
    invalid_type_error: 'La moneda debe ser una cadena de caracteres.',
    required_error: 'Debes ingresar la moneda con la cual se hara el pago',
  }),
  exchange_rate: z.string({
    invalid_type_error: 'El tipo de cambio debe ser un numero',
    required_error:
      'Debes ingresar el tipo de cambio con el que se hara el pago',
  }),
  type: z.string({
    invalid_type_error: 'El tipo de pago debe ser una cadena de caracteres.',
    required_error: 'Debes ingresar el tipo de pago',
  }),
  date: z.date(),
});
export type PaymentData = z.infer<typeof PaymentFormSchema>;
