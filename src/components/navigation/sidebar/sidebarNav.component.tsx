'use client';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { cn } from '@/lib/utils';
import { buttonVariants } from '@/components/ui/button.component';

interface SidebarNavProps extends React.HTMLAttributes<HTMLElement> {
  items: {
    href: string;
    title: string;
  }[];
}

export function SidebarNav({ items }: SidebarNavProps) {
  const pathname = usePathname();

  return (
    <nav className="grow overflow-y-auto flex flex-col lg:space-y-1 py-4">
      {items.map((item) => (
        <Link
          key={item.href}
          href={item.href}
          className={cn(
            buttonVariants({ variant: 'ghost', size: 'lg' }),
            pathname === item.href
              ? 'text-primary hover:bg-muted'
              : 'hover:bg-muted',
            'justify-start',
            'rounded-none'
          )}
        >
          {item.title}
        </Link>
      ))}
    </nav>
  );
}
