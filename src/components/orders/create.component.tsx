import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from '@/components/ui/card.component';

import { OrdersForm } from '../forms/ordersForm.component';

export function CreateOrder() {
  return (
    <section className="container bg-background w-full min-h-screen flex items-center justify-center flex-col space-y-6">
      <Card>
        <CardHeader>
          <CardTitle>Crea un nueva Orden</CardTitle>
          <CardDescription>
            Ingresa la información de la orden que deseas crear
          </CardDescription>
        </CardHeader>

        <CardContent>
          <OrdersForm />
        </CardContent>
      </Card>
    </section>
  );
}
