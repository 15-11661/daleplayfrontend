'use client';

import { ProductsList } from './products.component';
import { useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { ProductsService } from '@/services/products/products.service';
import { Button } from '../ui/button.component';
import { Title } from '../typography/title.component';

export type PageInfo = { page: string; pageSize: string };

const productsService = new ProductsService();

export function PaginatedProductsList() {
  const token = '';
  const [pageInfo, setPageInfo] = useState({ page: '1', pageSize: '10' });

  const productsQuery = useQuery({
    queryKey: ['products', token, pageInfo],
    queryFn: async () => productsService.getAll(token, { ...pageInfo }),
  });

  const disabledPrevButton = Number.parseInt(pageInfo.page) === 1;
  const disabledNextButton =
    productsQuery.data &&
    Number.parseInt(pageInfo.page) === productsQuery.data.totalPages;

  const handleOnPreviousPage = () => {
    if (disabledPrevButton) return;

    setPageInfo((oldValue) => ({
      ...oldValue,
      page: `${Number.parseInt(oldValue.page) - 1}`,
    }));
  };

  const handleOnNextPage = () => {
    if (disabledNextButton) return;

    setPageInfo((oldValue) => ({
      ...oldValue,
      page: `${Number.parseInt(oldValue.page) + 1}`,
    }));
  };

  return (
    <>
      {productsQuery.isLoading && <Title>Cargando productos...</Title>}

      {!productsQuery.isLoading && !!productsQuery.data && (
        <ProductsList paginatedProducts={productsQuery.data} />
      )}

      <section className="mx-auto flex justify-center space-x-4 mt-16">
        <Button disabled={disabledPrevButton} onClick={handleOnPreviousPage}>
          Anterior
        </Button>
        <Button disabled={disabledNextButton} onClick={handleOnNextPage}>
          Siguiente
        </Button>
      </section>
    </>
  );
}
