'use client';

import { ProductsService } from '@/services/products/products.service';
import { useQuery } from '@tanstack/react-query';
import Image from 'next/image';
import { ProductModel } from '@/models/product.model';
import { useState } from 'react';
import { useEffect } from 'react';
import { useToken } from '@/stores/auth/auth.store';
import { Input } from '@/components/ui/input.component';
import { Button } from '@/components/ui/button.component';

interface ProductDetailProps {
  productId: string;
}

export function ProductDetail({ productId }: ProductDetailProps) {
  const token = useToken() || '';
  const productService = new ProductsService();
  const { data: product } = useQuery({
    queryKey: ['product', productId],
    queryFn: async () => productService.getById(productId, token),
  });

  const [editedProduct, setEditedProduct] = useState<ProductModel>({
    id: 0,
    category: 0,
    description: '',
    name: '',
    stock: 0,
    leasePrice: 0,
    initialFee: 0,
    cashPrice: 0,
    brand: '',
    internalId: '',
    profileImage: '',
    extra: {},
    leaseOptions: {},
    images: [],
    createdAt: '',
    isFeatured: false,
  });

  const [isEditing, setIsEditing] = useState(false);
  const [originalProduct, setOriginalProduct] = useState<ProductModel>({
    id: 0,
    category: 0,
    description: '',
    name: '',
    stock: 0,
    leasePrice: 0,
    initialFee: 0,
    cashPrice: 0,
    brand: '',
    internalId: '',
    profileImage: '',
    extra: {},
    leaseOptions: {},
    images: [],
    createdAt: '',
    isFeatured: false,
  });

  const [productImages, setProductImages] = useState<File>();
  const [imgError, setImgaError] = useState<Boolean>(false);

  const [weeklyFee, setWeeklyFee] = useState<number>(0);

  useEffect(() => {
    const loadProduct = async () => {
      const productService = new ProductsService();
      try {
        const product = await productService.getById(productId, token);
        setEditedProduct(product);
        setOriginalProduct(product);

        const newWeeklyFee = product.leasePrice / 24;
        setWeeklyFee(newWeeklyFee);

        const currentProductName = product.name.toLowerCase();

        setEditedProduct({
          ...product,
          name: currentProductName,
        });
        const nameProduct = product.name.toLowerCase();
      } catch (error) {
        console.error('Error al cargar el producto', error);
      }
    };

    loadProduct();
  }, [productId, token]);

  const handleInputChange = (field: string, value: any) => {
    setEditedProduct({
      ...editedProduct,
      [field]: value,
    });

    if (field === 'leasePrice') {
      const newWeeklyFee = parseFloat(value) / 24;
      setWeeklyFee(newWeeklyFee);
    }
  };

  const handleImageInputChange = (e: React.FormEvent<HTMLInputElement>) => {
    const files = e.currentTarget.files;
    setProductImages(files![0]);
  };

  const handleSave = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      const updatedProductData = {
        internalId: editedProduct.internalId,
        description: editedProduct.description,
        cashPrice: editedProduct.cashPrice,
        initialFee: editedProduct.initialFee,
        stock: editedProduct.stock,
        leasePrice: editedProduct.leasePrice,
      };

      await productService.patch(productId, token, updatedProductData);

      // Subir imagen nueva
      await productService.postImage(
        token,
        productImages!,
        editedProduct.id.toString()
      );

      setIsEditing(false);
    } catch (error) {
      console.error('Error al actualizar el producto', error);
    }
  };

  const handleCancel = () => {
    setEditedProduct(originalProduct);
    setIsEditing(false);
  };

  return (
    <main>
      {product && (
        <>
          <header>
            <h1 className="font-bold text-3xl pb-4">
              {product.name} - {product.brand}
            </h1>
          </header>
          <section className="lg:grid lg:grid-cols-2">
            <form onSubmit={handleSave} className="space-y-3 text-lg">
              <div className="col-span-2 grid grid-cols-2 gap-2">
                <div className="col-span-1">
                  <label htmlFor="description" className="block text-sm">
                    Descripción
                  </label>
                </div>
                <div className="col-span-1">
                  <Input
                    type="text"
                    id="description"
                    value={editedProduct.description}
                    onChange={(e) =>
                      handleInputChange('description', e.target.value)
                    }
                  />
                </div>
                <div className="col-span-1">
                  <label htmlFor="cashPrice" className="block text-sm">
                    Precio de Contado
                  </label>
                </div>
                <div className="col-span-1">
                  <Input
                    type="number"
                    id="cashPrice"
                    value={editedProduct.cashPrice}
                    onChange={(e) =>
                      handleInputChange('cashPrice', e.target.value)
                    }
                  />
                </div>
                <div className="col-span-1">
                  <label htmlFor="initialFee" className="block text-sm">
                    Cuota inicial
                  </label>
                </div>
                <div className="col-span-1">
                  <Input
                    type="number"
                    id="initialFee"
                    value={editedProduct.initialFee}
                    onChange={(e) =>
                      handleInputChange('initialFee', e.target.value)
                    }
                  />
                </div>
                <div className="col-span-1">
                  <label htmlFor="stock" className="block text-sm">
                    Cantidad disponible
                  </label>
                </div>
                <div className="col-span-1">
                  <Input
                    type="number"
                    id="stock"
                    value={editedProduct.stock}
                    onChange={(e) => handleInputChange('stock', e.target.value)}
                  />
                </div>
                <div className="col-span-1">
                  <label htmlFor="leasePrice" className="block text-sm">
                    Precio de arrendamiento
                  </label>
                </div>
                <div className="col-span-1">
                  <Input
                    type="number"
                    id="leasePrice"
                    value={editedProduct.leasePrice}
                    onChange={(e) =>
                      handleInputChange('leasePrice', e.target.value)
                    }
                  />
                </div>
                <div className="col-span-1">
                  <label htmlFor="weeklyFee" className="block text-sm">
                    Cuota Semanal
                  </label>
                </div>
                <div className="col-span-1">
                  <Input
                    type="number"
                    id="weeklyFee"
                    value={weeklyFee}
                    disabled
                  />
                </div>
                <div className="col-span-1">
                  <label htmlFor="productImages" className="block text-sm">
                    Imagenes
                  </label>
                </div>
                <div className="col-span-1">
                  <Input
                    type="file"
                    id="productImages"
                    accept="image/png, image/jpg, image/jpeg"
                    onChange={handleImageInputChange}
                  />
                  {imgError && (
                    <div className=" bg-red-300 rounded-lg my-2 text-center">
                      <span className="text-red-700 text-sm ">
                        Solo se pueden guardar 5 imagenes
                      </span>
                    </div>
                  )}
                </div>
              </div>
              <div className="text-center">
                <Button onClick={handleCancel}>Cancelar</Button>
                <Button type="submit" className="ml-4">
                  Guardar Cambios
                </Button>
              </div>
            </form>
            <div>
              <figure className="">
                <Image
                  src="https://www.mototransformerempirekeeway.com/wp-content/uploads/2021/09/arsen-II-motos-transformer.jpg"
                  alt="Imagen de moto negra marca Empire"
                  width={915}
                  height={610}
                  className="w-full rounded"
                />
              </figure>
            </div>
          </section>
        </>
      )}
    </main>
  );
}
