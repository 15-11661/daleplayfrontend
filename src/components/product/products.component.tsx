'use client';
import { ProductsService } from '@/services/products/products.service';
import { ProductCard } from './productCard.component';
import { Pagination } from '@/models/pagination.model';
import { ProductModel } from '@/models/product.model';

const productsService = new ProductsService();

interface ProductsListProps {
  paginatedProducts: Pagination<ProductModel>;
}

export function ProductsList({ paginatedProducts }: ProductsListProps) {
  const { results: products } = paginatedProducts;

  return (
    <div className="py-3">
      <div className="flex flex-wrap justify-center md:justify-start gap-6">
        {products.map((product) => (
          <ProductCard product={product} key={product.id} />
        ))}
      </div>
    </div>
  );
}
