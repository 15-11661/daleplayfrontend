'use client';

import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from '@/components/ui/card.component';
import { ProductForm } from '../forms/productForm.component';

export function CreateProduct() {
  return (
    <section className="container bg-background w-full min-h-screen flex items-center justify-center flex-col space-y-6">
      <Card>
        <CardHeader>
          <CardTitle>Crea un nuevo producto</CardTitle>
          <CardDescription>
            Ingresa la información del producto que deseas crear
          </CardDescription>
        </CardHeader>

        <CardContent>
          <ProductForm />
        </CardContent>
      </Card>
    </section>
  );
}
