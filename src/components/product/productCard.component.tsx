import {
  Card,
  CardTitle,
  CardContent,
  CardFooter,
  CardDescription,
} from '@/components/ui/card.component';
import { ProductModel } from '@/models/product.model';
import Image from 'next/image';
import Link from 'next/link';

interface ProductCardProps {
  product: ProductModel;
}

export function ProductCard({ product }: ProductCardProps) {
  return (
    <Card className="w-full md:w-[250px] lg:w-[300px] hover:cursor-pointer  hover:bg-muted hover:text-muted-foreground hover:scale-105 transition-all">
      <Link href={`/dashboard/products/${product.internalId}`}>
        <CardContent>
          <Image
            src={product.profileImage}
            alt={product.description}
            width={915}
            height={610}
            className="pt-6"
          />
        </CardContent>
        <CardFooter className="flex flex-col items-start space-y-2">
          <CardTitle>{product.name}</CardTitle>
          <CardDescription>
            {product.brand} - {product.description}
          </CardDescription>
        </CardFooter>
      </Link>
    </Card>
  );
}
