import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from '@/components/ui/card.component';

import { PaymentForm } from '../forms/paymentForm';

export function CreatePayment() {
  return (
    <section className="container bg-background w-full min-h-screen flex items-center justify-center flex-col space-y-6">
      <Card>
        <CardHeader>
          <CardTitle>Crea un nuevo Pago</CardTitle>
          <CardDescription>
            Ingresa la información del pago que deseas crear
          </CardDescription>
        </CardHeader>

        <CardContent>
          <PaymentForm />
        </CardContent>
      </Card>
    </section>
  );
}
