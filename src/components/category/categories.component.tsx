import React, { useState } from 'react';
import { CategoryService } from '@/services/categories/categories.service';
import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { useToken } from '@/stores/auth/auth.store';
import { TrashIcon } from '@/app/icons/trash';
import { EditIcon } from '@/app/icons/edit';
import { Button } from '../ui/button.component';

const categoriesService = new CategoryService();

export function Categories() {
  const token = useToken() || '';
  const queryClient = useQueryClient();

  const categoriesQuery = useQuery(['categories'], async () =>
    categoriesService.getAll(token)
  );

  const categories = categoriesQuery.data;

  const deleteCategoryMutation = useMutation((categoryId: string) =>
    categoriesService.deleteCategory(token, categoryId)
  );

  const [editingCategoryId, setEditingCategoryId] = useState<string | null>(
    null
  );
  const [editedCategoryName, setEditedCategoryName] = useState('');

  const openEditPopup = (categoryId: string, categoryName: string) => {
    setEditingCategoryId(categoryId);
    setEditedCategoryName(categoryName);
  };

  const closeEditPopup = () => {
    setEditingCategoryId(null);
    setEditedCategoryName('');
  };

  const handleEditCategory = async () => {
    try {
      if (editingCategoryId !== null) {
        await categoriesService.updateCategory(token, editingCategoryId, {
          name: editedCategoryName,
        });
        queryClient.invalidateQueries(['categories']);
        closeEditPopup();
      }
    } catch (error) {
      console.error('Error al editar la categoría', error);
    }
  };

  const handleDeleteCategory = async (categoryId: string) => {
    try {
      await deleteCategoryMutation.mutateAsync(categoryId);
      queryClient.invalidateQueries(['categories']);
    } catch (error) {
      console.error('Error al eliminar la categoría', error);
    }
  };

  return (
    <section className="pt-4">
      {categories && (
        <>
          {categories.map((category) => (
            <div
              key={category.id}
              className="flex justify-between items-center mb-2"
            >
              <h1 className="mr-2">{category.name}</h1>
              <div className="flex space-x-2">
                <button
                  className="text-sm"
                  onClick={() => openEditPopup(category.id, category.name)}
                >
                  <EditIcon />
                </button>
                <button onClick={() => handleDeleteCategory(category.id)}>
                  <TrashIcon />
                </button>
              </div>
            </div>
          ))}
        </>
      )}

      {editingCategoryId !== null && (
        <div className="fixed top-0 left-0 right-0 bottom-0 flex justify-center items-center bg-gray-500 bg-opacity-50">
          <div className="bg-white p-4 rounded-md">
            <h2 className="text-lg font-semibold mb-2">Editar Categoría</h2>
            <input
              type="text"
              className="border border-gray-300 rounded-md p-2 mb-2"
              value={editedCategoryName}
              onChange={(e) => setEditedCategoryName(e.target.value)}
            />
            <Button className="mx-2" onClick={handleEditCategory}>
              Aceptar
            </Button>
            <button
              className="bg-gray-300 text-gray-700 px-4 py-2 rounded-md"
              onClick={closeEditPopup}
            >
              Cancelar
            </button>
          </div>
        </div>
      )}
    </section>
  );
}
