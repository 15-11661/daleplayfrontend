'use client';

import { useRouter } from 'next/navigation';
import { ReactElement, useEffect, useState } from 'react';
import { useToast } from '../ui/use-toast';
import { Title } from '../typography/title.component';
import { useAuthActions, useAuthService } from '@/stores/auth/auth.store';

interface RoutesProtectionProviderProps {
  children: ReactElement;
}

export function RoutesProtectionProvider({
  children,
}: RoutesProtectionProviderProps) {
  const toast = useToast();
  const router = useRouter();
  const [isValid, setIsValid] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const authService = useAuthService();
  const authActions = useAuthActions();

  const checkUserAuthentication = async () => {
    try {
      const token = localStorage.getItem('TOKEN') ?? '';

      const currentUser = await authService.getCurrentUser(token);

      if (!currentUser) {
        toast.toast({
          title: 'Sesión inválida',
          description:
            'Debes iniciar sesión antes de poder utilizar esta aplicación.',
          variant: 'destructive',
        });

        setIsValid(false);

        return;
      }

      authActions.setUser(currentUser);

      setIsValid(true);
    } catch (error: any) {
      setIsValid(false);

      toast.toast({
        title: 'Sesión inválida',
        description:
          error?.mesage || 'Tuvimos problemas para validar tu sesión',
        variant: 'destructive',
      });
      router.replace('/');
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    checkUserAuthentication();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isLoading) {
    return (
      <div className="fixed w-full h-screen bg-primary p-2.5">
        <div className="w-full h-full flex items-center justify-center rounded-md bg-white">
          <Title>Validando la sesión del usuario</Title>
        </div>
      </div>
    );
  }

  if (!isValid && !isLoading) return <></>;

  return <>{children}</>;
}
