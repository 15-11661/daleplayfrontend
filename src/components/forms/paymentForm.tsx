'use client';
import * as z from 'zod';
import { useRouter } from 'next/navigation';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form.component';
import { Input } from '@/components/ui/input.component';
import { Button } from '@/components/ui/button.component';
import { PaymentFormSchema as formSchema } from '@/schemas/payment.schema';
import React from 'react';
import { useToast } from '../ui/use-toast';
import { ProductsService } from '@/services/products/products.service';
import { useToken } from '@/stores/auth/auth.store';
import { PaymentData } from '@/services/products/types/CreatePayment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

type FormValues = z.infer<typeof formSchema>;

const productService = new ProductsService();

export function PaymentForm() {
  const toast = useToast();
  const token = useToken();
  const router = useRouter();
  const leaseId = window.location.search.replace('?leaseId=', '');
  const currentDate = new Date();

  const form = useForm<FormValues>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      amount: '',
      currency: '',
      exchange_rate: '',
      type: '',
      date: currentDate,
    },
  });

  const onSubmit = async (values: FormValues) => {
    if (!token) return;

    try {
      const paymentData: PaymentData = {
        amount: values.amount,
        currency: values.currency,
        exchange_rate: values.exchange_rate,
        type: values.type,
        date: values.date,
      };

      if (typeof leaseId === 'string') {
        const response = await productService.createPayment(
          token,
          leaseId,
          paymentData
        );

        if ('errors' in response) {
          toast.toast({
            title: 'Error',
            description: 'Ocurrió un error al crear el pago',
            variant: 'destructive',
          });

          return;
        }

        toast.toast({
          title: 'Éxito',
          description: 'Pago creado correctamente',
        });
        router.push('/dashboard/payments');
      } else {
        console.error('leaseId no es una cadena válida');
      }
    } catch (err) {
      toast.toast({
        title: 'Error',
        description: 'Ocurrió un error al crear el Pago',
        variant: 'destructive',
      });
    }
  };

  return (
    <div>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="w-full space-y-4 p-4 rounded"
        >
          <FormField
            control={form.control}
            name="amount"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Monto</FormLabel>
                <FormControl>
                  <Input id="amount" type="text" placeholder="400" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="currency"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Moneda</FormLabel>
                <FormControl>
                  <Input
                    id="currency"
                    type="text"
                    placeholder="USD"
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="exchange_rate"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Exchange Rate</FormLabel>
                <FormControl>
                  <Input
                    id="exchange_rate"
                    type="text"
                    placeholder="2"
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="type"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Tipo de Pago</FormLabel>
                <FormControl>
                  <Input id="type" type="text" placeholder="cash" {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="date"
            render={({ field }) => (
              <FormItem>
                <div className="flex flex-col">
                  <FormLabel className="my-2">Fecha</FormLabel>
                  <DatePicker
                    selected={new Date(field.value)}
                    onChange={(date) => field.onChange(date)}
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={15}
                    dateFormat="yyyy-MM-dd HH:mm:ss"
                  />
                </div>
                <FormMessage />
              </FormItem>
            )}
          />

          <Button type="submit" className="w-full">
            Crear
          </Button>
        </form>
      </Form>
    </div>
  );
}
