'use client';
import * as z from 'zod';
import '@/components/componentStyle.css'
import Image from 'next/image';
import { useState } from 'react';
import { useRouter } from 'next/navigation';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form.component';
import { Input } from '@/components/ui/input.component';
import { Button } from '@/components/ui/button.component';
import { loginFormSchema as formSchema } from '@/schemas/login.schema';
import React from 'react';
import { useToast } from '../ui/use-toast';
import { useAuthActions, useAuthService } from '@/stores/auth/auth.store';
import showPswrdIcon from '@/../public/password-visible.svg'
import hidePswrdIcon from '@/../public/password-not-visible.svg'

type FormValues = z.infer<typeof formSchema>;

export function LoginForm() {
  const authService = useAuthService();
  const { setUser } = useAuthActions();
  const [showPassword, setShowPassword] = useState(false);
  const toast = useToast();
  const router = useRouter();
  const form = useForm<FormValues>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      username: '',
      password: '',
    },
  });

  const onSubmit = async (values: FormValues) => {
    try {
      //console.log("Valores:", values);
      const res = await authService.login({
        username: values.username,
        password: values.password,
      });
      //console.log("Backend Respuesta:", res);
      
      if (res){
        // Validar si es Administrador
        if (res.isSuperuser === true) {
          //console.log("User is a superuser:", res.isSuperuser);
          toast.toast({
            title: 'Bienvenido Administrador',
            description: 'Has iniciado sesión como Administrador',
          });
          // Redirigir a un dashboard para Administrador
          setUser(res);
          router.push('/dashboard');
        } else {
          //console.log("User is not a superuser:", res.isSuperuser);
          toast.toast({
            title: 'Bienvenido',
            description: 'Has iniciado sesión correctamente',
          });
          // Redirigir a un landing para usuarios
          setUser(res);
          router.push('/landing');
        }
      }
    } catch (err) {
     //console.error("Error during login:", err);
      toast.toast({
        title: 'Error',
        description: 'Usuario o contraseña incorrectos',
        variant: 'destructive',
      });
    }
  };

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className=" w-full space-y-4 p-4 rounded"
      >
        <FormField
          control={form.control}
          name="username"
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Input
                  id="username"
                  type="text"
                  autoCorrect="off"
                  autoComplete="username"
                  autoCapitalize="none"
                  placeholder="Nombre de usuario"
                  className="white-input"
                  {...field}
                  onFocus={(e) => {
                    if (e.target.value === 'nombre de usuario') {
                      form.setValue('username', '');
                    }
                  }}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="password"
          render={({ field }) => (
            <FormItem>
              <FormControl>
              <div className='password-input-container'>
                <Input
                  id="password"
                  type={showPassword ? 'text' : 'password'}
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="Contraseña"
                  className="white-input"
                  {...field}
                  onFocus={(e) => {
                    if (e.target.value === 'contraseña') {
                      form.setValue('password', '');
                    }
                  }}
                />
                <div
                  className="eye-icon" 
                  onClick={() => setShowPassword(!showPassword)}>
                    { showPassword ? 
                    (<img src='/password-not-visible.svg' alt='Ocultar Contraseña'/>) : 
                    (<img src='/password-visible.svg' alt="Mostrar Contraseña"/>) }
                </div>
              </div>
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <Button type="submit" className="w-full">
          Iniciar sesión
        </Button>
      </form>
    </Form>
  );
}