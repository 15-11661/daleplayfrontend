//ordersForm.component.tsx
//src compponents forms
'use client';
import * as z from 'zod';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import React from 'react';
import { useToast } from '../ui/use-toast';
import { ProductsService } from '@/services/products/products.service';
import { useToken } from '@/stores/auth/auth.store';
import { LeaseCreateData } from '@/models/lease.model';
import { MySchema as formSchema } from '@/schemas/orders.schema';
import { useEffect } from 'react';

import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form.component';

import { Input } from '@/components/ui/input.component';
import { Button } from '@/components/ui/button.component';
import { useRouter } from 'next/navigation';

type FormValues = z.infer<typeof formSchema>;
const productService = new ProductsService();



export function OrdersForm() {
  const toast = useToast();
  const router = useRouter();
  const token = useToken();

  
  const form = useForm<FormValues>({
    resolver: zodResolver(formSchema),
     defaultValues: {
      company: {
        name: '',
        web_page: '',
        instagram: '',
        facebook: '',
        address: '',
      },
      loan_grantor: {
        first_name: '',
        last_name: '',
        relationship: '',
        phone_number: '',
        email: '',
        address_room: '',
        land_line_number: '',
      },
      lease: {
        country_user_id: '',
        type: '',
        state: '',
        user_score: '',
        product: '',
        full_product_price: '',
        initial_fee: '',
        monthly_fee: '',
        fees_number: '',
        address: '',
        lease_reason: '',
      },
    },
  });


  const { control, watch, setValue } = form;

  const initialFee = watch('lease.initial_fee');
  const monthlyFee = watch('lease.monthly_fee');
  const feesNumber = watch('lease.fees_number');

  useEffect(() => {
    if (initialFee && monthlyFee && feesNumber) {
      const totalProductPrice =
        parseFloat(initialFee) +
        parseFloat(monthlyFee) * parseFloat(feesNumber);
      setValue('lease.full_product_price', totalProductPrice.toFixed(2));
    }
  }, [initialFee, monthlyFee, feesNumber, setValue]);

  const onSubmit = async (values: FormValues) => {
    if (!token) return;

    try {
      const LeaseData: LeaseCreateData = {
        company: {
          name: values.company.name,
          web_page: values.company.web_page,
          instagram: values.company.instagram,
          facebook: values.company.facebook,
          address: values.company.address,
        },
        loanGrantor: {
          first_name: values.loan_grantor.first_name,
          last_name: values.loan_grantor.last_name,
          relationship: values.loan_grantor.relationship,
          phone_number: values.loan_grantor.phone_number,
          email: values.loan_grantor.email,
          address_room: values.loan_grantor.address_room,
          land_line_number: values.loan_grantor.land_line_number,
        },
        lease: {
          country_user_id: values.lease.country_user_id,
          type: values.lease.type,
          state: values.lease.state,
          user_score: values.lease.user_score,
          product: values.lease.product,
          full_product_price: values.lease.full_product_price,
          initial_fee: values.lease.initial_fee,
          monthly_fee: values.lease.monthly_fee,
          fees_number: values.lease.fees_number,
          address: values.lease.address,
          lease_reason: values.lease.lease_reason,
        },
      };

      const response = await productService.createLease(token, LeaseData);

      if ('errors' in response) {
        toast.toast({
          title: 'Error',
          description: 'Ocurrió un error al crear el arrendamiento',
          variant: 'destructive',
        });

        return;
      }

      toast.toast({
        title: 'Éxito',
        description: 'Arrendamiento creado correctamente',
      });
      router.push('/dashboard/orders');
    } catch (err) {
      toast.toast({
        title: 'Error',
        description: 'Ocurrió un error al crear el arrendamiento',
        variant: 'destructive',
      });
    }
  };

  return (
    <div>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <h1 className="my-4 font-bold  text-blue-500">
            Datos de la Compañia
          </h1>
          <FormField
            control={form.control}
            name="company.name"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Nombre de la Compañía</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="companyName"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione el nombre
                      </option>
                      <option value="daleplay">daleplay</option>
                    </select>
                  </div>
                  
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="company.web_page"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Ingresa la pag web de la Compañia</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="webPage"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione la página
                      </option>
                      <option value="https://www.daleplay.com">https://www.daleplay.com</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="company.instagram"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Instagram</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="instagram"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione el instagram
                      </option>
                      <option value="@daleplay">@daleplay</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="company.facebook"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Facebook</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="facebook"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione el facebook
                      </option>
                      <option value="@daleplay">@daleplay</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="company.address"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Dirección de la Compañía</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="company.address"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione dirección
                      </option>
                      <option value="caracas">caracas</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <h1 className=" mt-12 mb-4 font-bold text-blue-500">
            Datos del fiador
          </h1>
          <FormField
            control={form.control}
            name="loan_grantor.first_name"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Primer Nombre del Fiador</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="first_name"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione el nombre
                      </option>
                      <option value="daleplay">daleplay</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="loan_grantor.last_name"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Apellido del Fiador</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="last_name"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione apellido
                      </option>
                      <option value="daleplay">daleplay</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="loan_grantor.relationship"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Relación con el fiador</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="relationship"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione la relación
                      </option>
                      <option value="daleplay">daleplay</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="loan_grantor.phone_number"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Número de Teléfono del fiador</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="loan_grantor.phone_number"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione el número
                      </option>
                      <option value="00000000000">00000000000</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="loan_grantor.email"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Correo Electrónico del fiador</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="email"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione el correo
                      </option>
                      <option value="daleplay@gmail.com">daleplay</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="loan_grantor.address_room"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Dirección de Habitación del fiador</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="address_room"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione direccion
                      </option>
                      <option value="daleplay">daleplay</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="loan_grantor.land_line_number"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Número de Teléfono Fijo del fiador</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="land_line_number"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione el número
                      </option>
                      <option value="00000000000">00000000000</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <h1 className="mt-12 mb-4 font-bold text-blue-500">
            Datos del Arrendamiento
          </h1>
          <FormField
            control={form.control}
            name="lease.country_user_id"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Cédula</FormLabel>
                <FormControl>
                  <Input
                    id="lease.country_user_id"
                    type="text"
                    autoCorrect="off"
                    autoComplete="off"
                    autoCapitalize="none"
                    placeholder="Cedula"
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="lease.type"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Tipo de Arrendamiento</FormLabel>
                <FormControl>
                  
                      {/* <option value="" disabled> */}
                        {/* Seleccione el tipo */}
                      <Input id="type" type="text" placeholder="lease" {...field} />
                      {/* </option>
                      <option value="lease">lease</option> */}
                              
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          {/* <FormField
            control={form.control}
            name="lease.state"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Estado del Arrendamiento</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="state"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione estado
                      </option>
                      <option value="ACTIVE">ACTIVE</option>
                      <option value="PENDING APPROVAL">PENDING APPROVAL</option>
                      <option value="CANCELED">CANCELED</option>
                      <option value="FINISHED">FINISHED</option>
                      <option value="REJECTED">REJECTED</option>
                    </select>
                  </div> 
                  
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          /> */}

          <FormField
            control={form.control}
            name="lease.user_score"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Puntuación del Usuario</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="user_score"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione el valor
                      </option>
                      <option value="1">1</option>
                    </select>
                  </div> 
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="lease.product"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Producto</FormLabel>
                <FormControl>
                  <Input
                    id="product"
                    type="text"
                    autoCorrect="off"
                    autoComplete="off"
                    autoCapitalize="none"
                    placeholder="Id del Producto"
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="lease.initial_fee"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Costo de la Cuota Inicial</FormLabel>
                <FormControl>
                  <Input
                    id="initial_fee"
                    type="text"
                    autoCorrect="off"
                    autoComplete="off"
                    autoCapitalize="none"
                    placeholder="Costo de la Cuota Inicial"
                     // prueba
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="lease.monthly_fee"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Cuota Mensual</FormLabel>
                <FormControl>
                  <Input
                    id="monthly_fee"
                    type="text"
                    autoCorrect="off"
                    autoComplete="off"
                    autoCapitalize="none"
                    placeholder="Cuota Mensual"
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="lease.fees_number"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Número de Cuotas</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="fees_number"
                      {...field}
                      className="px-3 py-2 border rounded-md" // Puedes ajustar las clases según tu diseño
                    >
                      <option value="" disabled>
                        Seleccione el número de cuotas
                      </option>
                      <option value="24">24</option>
                      <option value="36">36</option>
                      <option value="48">48</option>
                    </select>
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          {/* Campo autocompletado */}
          <FormField
            control={form.control}
            name="lease.full_product_price"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Precio Total del Producto</FormLabel>
                <FormControl>
                  <Input
                    id="full_product_price"
                    type="text"
                    autoCorrect="off"
                    autoComplete="on"
                    autoCapitalize="none"
                    placeholder="Precio Total del Producto"
                    {...field}
                    readOnly
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          

          <FormField
            control={form.control}
            name="lease.address"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Dirección del Arrendamiento</FormLabel>
                <FormControl>
                  <Input
                    id="address"
                    type="text"
                    autoCorrect="off"
                    autoComplete="on"
                    autoCapitalize="none"
                    placeholder="Dirección del Arrendamiento"
                    {...field}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="lease.lease_reason"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Razón del Arrendamiento</FormLabel>
                <FormControl>
                  <div className="flex flex-col">
                    <select
                      id="lease_reason"
                      {...field}
                      className="px-3 py-2 mt-2 border rounded-md"
                    >
                      <option value="" disabled>
                        Seleccione la razon
                      </option>
                      <option value="Daleplay">Daleplay</option>
                    </select>
                  </div> 
                  
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <Button type="submit" className="w-full mt-4">
            Crear
          </Button>
        </form>
      </Form>
    </div>
  );
}
