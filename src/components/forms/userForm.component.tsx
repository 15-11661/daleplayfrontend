'use client';
import * as z from 'zod';
import { useRouter } from 'next/navigation';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form.component';
import { Input } from '@/components/ui/input.component';
import { Button } from '@/components/ui/button.component';
import { UserFormSchema as formSchema } from '@/schemas/user.schema';
import React from 'react';
import { useToast } from '../ui/use-toast';
import { UserService } from '@/services/user/user.service';
import { useToken } from '@/stores/auth/auth.store';
import { CreateNewUserData } from '@/services/user/types/CreateNewUser.type';

type FormValues = z.infer<typeof formSchema>;

const userService = new UserService();

export function UserForm() {
  const toast = useToast();
  const router = useRouter();
  const token = useToken();

  const form = useForm<FormValues>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      username: '',
      email: '',
      countryUserId: '',
      firstName: '',
      lastName: '',
      phoneNumber: '',
      password: '',
    },
  });

  const onSubmit = async (values: FormValues) => {
    if (!token) return;

    try {
      const userData: CreateNewUserData = {
        username: values.username,
        email: values.email,
        countryUserId: values.countryUserId,
        firstName: values.firstName,
        lastName: values.lastName,
        phoneNumber: values.phoneNumber,
        password: values.password,
      };

      const response = await userService.createUser(token, userData);

      if ('errors' in response) {
        toast.toast({
          title: 'Error',
          description: `El usuario o correo electronico estan repetidos`,
          variant: 'destructive',
        });

        return;
      }

      toast.toast({
        title: 'Éxito',
        description: 'Usuario creado correctamente',
      });

      router.push(`/dashboard/users/${response.data}`);
    } catch (error) {
      toast.toast({
        title: 'Error',
        description: `El usuario o correo electronico estan repetidos`,
        variant: 'destructive',
      });
    }
  };

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="space-y-4 w-full max-w-sm mx-auto"
      >
        <FormField
          control={form.control}
          name="username"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresa el nombre de usuario</FormLabel>
              <FormControl>
                <Input
                  id="username"
                  type="text"
                  autoCorrect="off"
                  autoComplete="name"
                  autoCapitalize="none"
                  placeholder=""
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="email"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresa el correo</FormLabel>
              <FormControl>
                <Input
                  id="email"
                  type="email"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder=""
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="firstName"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresar nombre</FormLabel>
              <FormControl>
                <Input
                  id="firstName"
                  type="text"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder=""
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="lastName"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresar apellido</FormLabel>
              <FormControl>
                <Input
                  id="lastName"
                  type="text"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder=""
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="countryUserId"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresar numero de Cedula</FormLabel>
              <FormControl>
                <Input
                  id="countryUserId"
                  type="text"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder=""
                  minLength={7}
                  pattern="[0-9]*"
                  maxLength={10}
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="phoneNumber"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresar número de teléfono</FormLabel>
              <FormControl>
                <Input
                  id="phoneNumber"
                  type="text"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder=""
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="password"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingrese la contraseña</FormLabel>
              <FormControl>
                <Input
                  id="password"
                  type="password"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="********"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <Button type="submit" className="w-full">
          Crear
        </Button>
      </form>
    </Form>
  );
}