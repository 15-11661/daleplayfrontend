'use client';
import * as z from 'zod';
import { useRouter } from 'next/navigation';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form.component';
import { Input } from '@/components/ui/input.component';
import { Button } from '@/components/ui/button.component';
import { ProductFormSchema as formSchema } from '@/schemas/product.schema';
import React from 'react';
import { useToast } from '../ui/use-toast';
import { ProductsService } from '@/services/products/products.service';
import { useToken } from '@/stores/auth/auth.store';
import { CreateNewProductData } from '@/services/products/types/CreateNewProduct.type';

type FormValues = z.infer<typeof formSchema>;

const productService = new ProductsService();

export function ProductForm() {
  const toast = useToast();
  const router = useRouter();
  const token = useToken();

  const form = useForm<FormValues>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: '',
      description: '',
      stock: '',
      brand: '',
      category: '',
      cashPrice: '',
      initialFee: '',
      leasePrice: '',
      extra: {
        width: '',
        height: '',
      },
    },
  });

  const onSubmit = async (values: FormValues) => {
    if (!token) return;

    try {
      const productData: CreateNewProductData = {
        name: values.name,
        description: values.description,
        stock: values.stock,
        brand: values.brand,
        category: values.category,
        cashPrice: values.cashPrice,
        initialFee: values.initialFee,
        leasePrice: values.leasePrice,
        extra: values.extra,
        isFeatured: false,
      };

      const response = await productService.create(token, productData);

      if ('errors' in response) {
        toast.toast({
          title: 'Error',
          description: 'Ocurrió un error al crear el producto',
          variant: 'destructive',
        });

        return;
      }

      toast.toast({
        title: 'Éxito',
        description: 'Producto creado correctamente',
      });

      router.push(`/dashboard/products/${response.data}`);
    } catch (err) {
      toast.toast({
        title: 'Error',
        description: 'Ocurrió un error al crear el producto',
        variant: 'destructive',
      });
    }
  };

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className=" w-full space-y-4 p-4 rounded"
      >
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresa el nombre del producto</FormLabel>
              <FormControl>
                <Input
                  id="name"
                  type="text"
                  autoCorrect="off"
                  autoComplete="name"
                  autoCapitalize="none"
                  placeholder="Motocicleta"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="description"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresa la descripción</FormLabel>
              <FormControl>
                <Input
                  id="description"
                  type="text"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="Motocicleta de 4 tiempos"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="stock"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresa el stock</FormLabel>
              <FormControl>
                <Input
                  id="stock"
                  type="string"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="4"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="brand"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ingresa la marca</FormLabel>
              <FormControl>
                <Input
                  id="brand"
                  type="text"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="Honda"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="category"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Selecciona la categoría</FormLabel>
              <FormControl>
                <Input
                  id="category"
                  type="string"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="1"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="cashPrice"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Precio de contado</FormLabel>
              <FormControl>
                <Input
                  id="cashPrice"
                  type="string"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="1000"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="initialFee"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Cuota inicial</FormLabel>
              <FormControl>
                <Input
                  id="initialFee"
                  type="string"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="500"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="leasePrice"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Precio de arrendamiento</FormLabel>
              <FormControl>
                <Input
                  id="leasePrice"
                  type="string"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="500"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="extra.width"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Ancho</FormLabel>
              <FormControl>
                <Input
                  id="extra.width"
                  type="string"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="100"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <FormField
          control={form.control}
          name="extra.height"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Alto</FormLabel>
              <FormControl>
                <Input
                  id="extra.height"
                  type="number"
                  autoCorrect="off"
                  autoCapitalize="none"
                  placeholder="100"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <Button type="submit" className="w-full">
          Crear
        </Button>
      </form>
    </Form>
  );
}
