import { GlikHttpService } from '@/services/glikHttp.service';
import {
  CreateNewUserData,
  CreateNewUserErrorResponse,
  CreateNewUserSuccessfullyResponse,
} from './types/CreateNewUser.type';


export class UserService {
  private service: GlikHttpService;

  constructor() {
    this.service = new GlikHttpService();
  }

  public async createUser(token: string, user: CreateNewUserData) { // deberia de haber una promesa (?)
    return this.service.post<CreateNewUserErrorResponse | CreateNewUserSuccessfullyResponse>({
      endpoint: '/user/all',
      customHeaders: {
        Authorization: `Token ${token}`,
      },
      data: {
        ...user,
      },
    });
  }
}
