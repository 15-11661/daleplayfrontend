import { GlikHttpService } from '../glikHttp.service';
import { CategoryModel } from '@/models/category.model';
import { GetAllServiceQueryParametersType } from '../products/types';
import { createUrl } from '@/utils/createUrl.utils';
import { GetCategoriesBackendResponseType } from './types/GetCategoriesBackendResponse.type';

export class CategoryService {
  private service: GlikHttpService;

  constructor() {
    this.service = new GlikHttpService();
  }

  public async getAll(
    token: string | undefined,
    queryParameters: GetAllServiceQueryParametersType = {}
  ): Promise<Array<CategoryModel>> {
    try {
      const url = createUrl({
        url: '/product/category/all',
        parameters: {
          pageSize: queryParameters?.pageSize ?? '10',
          page: queryParameters?.page ?? '1',
        },
      });

      const response = await this.service.get<GetCategoriesBackendResponseType>(
        {
          endpoint: url,
          token,
        }
      );

      return response.data;
    } catch (error) {
      console.log(error);
      return [];
    }
  }
  public async createCategory(
    token: string | undefined,
    categoryData: CategoryModel
  ): Promise<void> {
    try {
      const url = '/product/category/all';

      await this.service.post<GetCategoriesBackendResponseType>({
        endpoint: url,
        token,
        data: categoryData,
      });
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
  public async deleteCategory(
    token: string | undefined,
    categoryId: string
  ): Promise<void> {
    try {
      const url = `/product/category/${categoryId}`;

      await this.service.delete<void>({
        endpoint: url,
        token,
      });
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  public async updateCategory(
    token: string | undefined,
    categoryId: string,
    categoryData: Partial<CategoryModel>
  ): Promise<void> {
    try {
      const url = `/product/category/${categoryId}`;
      const requestData = {
        name: categoryData.name,
      };

      await this.service.patch<GetCategoriesBackendResponseType>({
        endpoint: url,
        token,
        data: requestData,
      });
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
