export interface GetCategoriesBackendResponseType {
  message: string;
  data: CategoryBackendResponse[];
}
export interface CategoryBackendResponse {
  id: string;
  name: string;
  parent: string | null;
}
