import type { Pagination } from '@/models/pagination.model';
import type { ProductModel } from '@/models/product.model';
import { GlikHttpService } from '@/services/glikHttp.service';
import type {
  GetAllServiceQueryParametersType,
  GetProductsBackendResponse as GetProductsBackendResponseType,
} from './types';
import { GetProductByIdBackendResponseType } from './types/GetProductByIdBackendResponse.type';
import { createUrl } from '@/utils/createUrl.utils';
import {
  CreateNewProductData,
  CreateNewProductErrorResponse,
  CreateNewProductSuccessfullyResponse,
} from './types/CreateNewProduct.type';
import { LeaseModel } from '@/models/lease.model';
import { CreatePaymentResponse, PaymentData } from './types/CreatePayment';
import {
  Payment,
  PaymentStatusChangeResponse,
  PaymentStatusChangeSuccessfully,
} from '../payments/types/GetAllpayments.type';
import { LeaseCreateData } from '@/models/lease.model';
import { LeaseStatusChangeSuccessfully } from './types/GetProductByIdBackendResponse.type';
import { LeaseStatusChangeResponse } from './types/GetProductByIdBackendResponse.type';
import { GetPaymentsBackendResponseType } from '../payments/types/GetAllpayments.type';
import { GetLeasesBackendResponseType } from './types/GetProductByIdBackendResponse.type';
const mockImage =
  'https://www.mototransformerempirekeeway.com/wp-content/uploads/2021/09/arsen-II-motos-transformer.jpg';

export class ProductsService {
  private service: GlikHttpService;

  constructor() {
    this.service = new GlikHttpService();
  }

  public async getAll(
    token: string | undefined,
    queryParameters: GetAllServiceQueryParametersType = {}
  ): Promise<Pagination<ProductModel>> {
    const response = await this.service.get<GetProductsBackendResponseType>({
      endpoint: createUrl({
        url: '/product/all',
        parameters: {
          pageSize: queryParameters?.pageSize ?? '10',
          page: queryParameters?.page ?? '1',
        },
      }),
      token,
    });

    const products = [...(response.data.results as ProductModel[])].map(
      (item) => ({
        ...item,
        profileImage: mockImage,
      })
    );

    return {
      ...response.data,
      results: products,
    } as Pagination<ProductModel>;
  }

  public async getById(
    id: string,
    token: string | undefined
  ): Promise<ProductModel> {
    const response = await this.service.get<GetProductByIdBackendResponseType>({
      endpoint: createUrl({
        url: `/product/name/${id}`,
      }),
      token,
    });

    const product: ProductModel = {
      ...response.data,
      profileImage: mockImage,
      images: [
        mockImage,
        mockImage,
        mockImage,
        mockImage,
        mockImage,
        mockImage,
      ],
    };

    return product;
  }

  public async create(token: string, product: CreateNewProductData) {
    return this.service.post<
      CreateNewProductErrorResponse | CreateNewProductSuccessfullyResponse
    >({
      endpoint: '/product/all',
      customHeaders: {
        Authorization: `Token ${token}`,
      },
      data: {
        ...product,
      },
    });
  }
  public async patch(
    id: string,
    token: string | undefined,
    productData: Partial<ProductModel>
  ): Promise<void> {
    await this.service.patch<void>({
      endpoint: `/product/name/${id}`,
      token,
      data: productData,
    });
  }

  public async createLease(
    token: string,
    leaseData: LeaseCreateData
  ): Promise<LeaseModel> {
    const response = await this.service.post<LeaseModel>({
      endpoint: '/lease/allcountryid',
      customHeaders: {
        Authorization: `Token ${token}`,
      },
      data: leaseData,
    });

    return response;
  }

  public async getAllLeases(
    token: string | undefined,
    queryParameters: GetAllServiceQueryParametersType = {}
  ): Promise<Pagination<LeaseModel>> {
    const response = await this.service.get<GetLeasesBackendResponseType>({
      endpoint: createUrl({
        url: '/lease/all',
        parameters: {
          pageSize: queryParameters?.pageSize ?? '10',
          page: queryParameters?.page ?? '1',
          search: queryParameters?.search ?? '',
        },
      }),
      token,
    });
    return {
      ...response.data,
    };
  }

  public async updateLeaseStatus(
    leaseId: number,
    newStatus: string,
    token: string
  ): Promise<LeaseStatusChangeResponse> {
    const response = await this.service.put<LeaseStatusChangeSuccessfully>({
      endpoint: `/lease/${leaseId}/status`,
      customHeaders: {
        Authorization: `Token ${token}`,
      },
      data: {
        new_status: newStatus,
      },
    });

    if (response) {
      return {
        message: 'Estado del arrendamiento actualizado con éxito',
        new_status: newStatus,
      };
    } else {
      return {
        message: 'Error al actualizar el estado del arrendamiento',
        new_status: '',
      };
    }
  }

  public async createPayment(
    token: string,
    leaseId: string,
    paymentData: PaymentData
  ) {
    return this.service.post<CreatePaymentResponse>({
      endpoint: `/payment/lease/${leaseId}`,
      customHeaders: {
        Authorization: `Token ${token}`,
      },
      data: paymentData,
    });
  }

  public async getAllPayments(
    token: string | undefined,
    queryParameters: GetAllServiceQueryParametersType = {}
  ): Promise<Pagination<Payment>> {
    const response = await this.service.get<GetPaymentsBackendResponseType>({
      endpoint: createUrl({
        url: '/payment/all',
        parameters: {
          pageSize: queryParameters?.pageSize ?? '10',
          page: queryParameters?.page ?? '1',
          search: queryParameters?.search ?? '',
        },
      }),
      token,
    });

    return {
      ...response.data,
    };
  }

  public async updatePaymentStatus(
    paymentId: number,
    newStatus: string,
    token: string
  ): Promise<PaymentStatusChangeResponse> {
    const response = await this.service.put<PaymentStatusChangeSuccessfully>({
      endpoint: `/payment/${paymentId}/status`,
      customHeaders: {
        Authorization: `Token ${token}`,
      },
      data: {
        new_status: newStatus,
      },
    });
    const adaptedResponse: PaymentStatusChangeResponse = {
      ...response,
      new_status: newStatus,
    };

    return adaptedResponse;
  }

  public async postImage(token: string, newImage: File, id: string) {
    // Crearmos un formData porque es necesario para enviar archivos al backend
    const formData = new FormData();

    formData.append('image', newImage, newImage.name);
    formData.append('product', id);

    // Es necesario especificar el header de Content-Type como multipart/form-data
    // Para que el backend pueda procesar el archivo
    const response = await this.service.post<
      CreateNewProductErrorResponse | CreateNewProductSuccessfullyResponse
    >({
      endpoint: `/product/image/all`,
      customHeaders: {
        Authorization: `Token ${token}`,
        'Content-Type': 'multipart/form-data',
      },
      data: formData,
    });

    return response;
  }
}
