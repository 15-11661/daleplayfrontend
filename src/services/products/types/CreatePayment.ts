export interface CreatePaymentResponse {
  message: string;
}

export interface PaymentData {
  amount: string;
  currency: string;
  exchange_rate: string;
  type: string;
  date: Date;
}
