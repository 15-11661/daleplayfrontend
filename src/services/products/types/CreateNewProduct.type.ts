export interface CreateNewProductData {
  name: string;
  isFeatured: boolean;
  description: string;
  category: string;
  stock: string;
  leasePrice: string;
  initialFee: string;
  cashPrice: string;
  brand: string;
  extra: object;
}

export interface CreateNewProductSuccessfullyResponse {
  message: string;
  data: string;
}

export interface CreateNewProductErrorResponse {
  message: string;
  errors: {
    [key: string]: string[];
  };
}
