import type { ProductBackendResponse } from './CommonBackendResponse.type';

export interface GetProductsBackendResponseType {
  message: string;
  data: BackendPagination;
}

export interface BackendPagination {
  currentPage: number;
  pageSize: number;
  totalElements: number;
  totalPages: number;
  results: ProductBackendResponse[];
}

export interface GetAllServiceQueryParametersType {
  page?: string;
  pageSize?: string;
  search?: string;
}
