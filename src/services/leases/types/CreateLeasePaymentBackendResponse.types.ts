export interface CreateLeasePaymentBackendResponse {
  message: string;
  data: unknown[];
}
