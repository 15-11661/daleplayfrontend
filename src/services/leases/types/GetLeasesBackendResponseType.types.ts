import { LeaseModel } from '@/models/lease.model';
import { Pagination } from '@/models/pagination.model';

export interface GetLeasesBackendResponseType {
  message: string;
  data: Pagination<LeaseModel>;
}
