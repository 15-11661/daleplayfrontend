export interface CreateLeaseBackendResponse {
  message: string;
  data: number;
}

export interface CreateLeaseBackendBody {
  company: Company;
  loanGrantor: LoanGrantor;
  lease: Lease;
}

export interface Company {
  name: string;
  webPage: string;
  instagram: string;
  facebook: string;
  address: string;
}

export interface LoanGrantor {
  firstName: string;
  lastName: string;
  relationship: string;
  phoneNumber: string;
  email: string;
  addressRoom: string;
  landLineNumber: string;
}

export interface Lease {
  country_user_id: string;
  type: string;
  state: string;
  userScore: number;
  product: number;
  fullProductPrice: number;
  initialFee: number;
  monthlyFee: number;
  feesNumber: number;
  address: string;
  leaseReason: string;
}
