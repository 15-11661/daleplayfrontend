export interface GetLeaseByIdBackendResponse {
  message: string;
  data: Data;
}

export interface Data {
  id: number;
  status: string;
  createdAt: string;
  user: number;
  userScore: number;
  product: number;
  fullProductPrice: number;
  initialFee: number;
  monthlyFee: number;
  feesNumber: number;
  address: string;
  rider: any;
  loanGrantor: number;
  leaseReason: string;
  payments: Payment[];
  nextPaymentDate: string;
  type: string;
}

export interface Payment {
  id: number;
  amount: number;
  createdAt: string;
  date: string;
  type: string;
  status: string;
  currency: string;
  exchangeRate: number;
  lease: Lease;
  user: User2;
}

export interface Lease {
  id: number;
  status: string;
  createdAt: string;
  type: string;
  userScore: number;
  fullProductPrice: number;
  initialFee: number;
  monthlyFee: number;
  feesNumber: number;
  leaseReason: string;
  user: User;
  product: Product;
  address: string;
  rider: any;
  loanGrantor: LoanGrantor;
}

export interface User {
  id: number;
  password: string;
  lastLogin: any;
  isSuperuser: boolean;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  isStaff: boolean;
  isActive: boolean;
  dateJoined: string;
  countryUserId: string;
  rif: any;
  phoneNumber: string;
  passportNumber: any;
  nationality: any;
  birthDate: any;
  type: string;
  forgotPasswordToken: any;
  groups: any[];
  userPermissions: any[];
}

export interface Product {
  id: number;
  internalId: string;
  name: string;
  description: string;
  stock: number;
  leasePrice: number;
  initialFee: number;
  cashPrice: number;
  brand: string;
  extra: Extra;
  createdAt: string;
  isFeatured: boolean;
  category: number;
}

export interface Extra {}

export interface Address {
  id: number;
  name: string;
  description: string;
  latitude: string;
  longitude: string;
  createdAt: string;
  isDeleted: boolean;
  user: number;
}

export interface LoanGrantor {
  id: number;
  firstName: string;
  lastName: string;
  relationship: string;
  phoneNumber: string;
  email: string;
  addressRoom: string;
  landLineNumber: string;
  company: number;
}

export interface Company {
  id: number;
  name: string;
  webPage: string;
  instagram: string;
  facebook: string;
  address: string;
}

export interface User2 {
  id: number;
  password: string;
  lastLogin: any;
  isSuperuser: boolean;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  isStaff: boolean;
  isActive: boolean;
  dateJoined: string;
  countryUserId: string;
  rif: any;
  phoneNumber: string;
  passportNumber: any;
  nationality: any;
  birthDate: any;
  type: string;
  forgotPasswordToken: any;
  groups: any[];
  userPermissions: any[];
}
