export const paymentStatusEnum = {
  PENDING: 'Pendiente por comprobar',
  PAID: 'Recibido',
  CANCELED: 'Rechazado',
};
