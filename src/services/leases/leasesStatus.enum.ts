export const leasesStatusEnum = {
  PENDING_APPROVAL: 'Esperando aprobación',
  ACTIVE: 'Aprobado',
  REJECTED: 'Rechazado',
  CANCELED: 'Cancelado',
  FINISHED: 'Finalizado',
};
