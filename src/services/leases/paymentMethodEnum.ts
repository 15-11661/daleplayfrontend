export const paymentMethodEnum = {
  CASH: 'Efectivo en dolares',
  BS_TRANSFER: 'Transferencia nacional',
  ZELLE: 'Zelle',
};
