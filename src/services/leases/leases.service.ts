import { CreateLeaseFormData } from '@/schemas/createLeaseForm.schema';
import { LeaseModel } from '@/models/lease.model';
import type { Pagination } from '@/models/pagination.model';
import { ProductModel } from '@/models/product.model';
import { GlikHttpService } from '@/services/glikHttp.service';
import { createUrl } from '@/utils/createUrl.utils';

import {
  CreateLeaseBackendBody,
  CreateLeaseBackendResponse,
} from './types/CreateLeaseBackendResponse.types';
import { CreateLeasePaymentBackendResponse } from './types/CreateLeasePaymentBackendResponse.types';
import { GetLeaseByIdBackendResponse } from './types/GetLeaseByIdBackendResponse.types';
import type { GetLeasesBackendResponseType } from './types/GetLeasesBackendResponseType.types';
import { LoanGrantor } from './types/GetLeaseByIdBackendResponse.types';
import { Company } from './types/GetLeaseByIdBackendResponse.types';

export class LeasesService {
  private service: GlikHttpService;

  constructor() {
    this.service = new GlikHttpService();
  }

  public async getAll(
    token: string | undefined
  ): Promise<Pagination<LeaseModel>> {
    const response = await this.service.get<GetLeasesBackendResponseType>({
      endpoint: createUrl({
        url: '/lease/user/all',
      }),
      token,
    });

    return {
      ...response.data,
    };
  }

  public async getAllLeasesByUser(
    userId: string | undefined,
    token: string | undefined
  ): Promise<LeaseModel[]> {
    const response = await this.service.get<GetLeasesBackendResponseType>({
      endpoint: `/lease/all?user=${userId}`,
      token,
    });

    if (response.data && Array.isArray(response.data.results)) {
      const leases = response.data.results.map((item: LeaseModel) => ({
        ...item,
      }));
      return leases;
    } else {
      throw new Error('No se pudieron obtener los arrendamientos.');
    }
  }

  public async getByUserAndId(
    userId: string | undefined,
    leaseId: number | undefined,
    token: string | undefined
  ): Promise<LeaseModel | null> {
    const userLeases = await this.getAllLeasesByUser(userId, token);
    if (userLeases && userLeases.length > 0) {
      const selectedLease = userLeases.find((lease) => {
        return lease.id === leaseId;
      });

      if (selectedLease) {
        return selectedLease;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  public async getLeaseById(token: string | undefined, leaseId: number) {
    const response = await this.service.get<GetLeaseByIdBackendResponse>({
      endpoint: createUrl({
        url: `/lease/user/${leaseId}`,
      }),
      token,
    });
    return {
      ...response.data,
    };
  }

  public async getLoanGrantorDetails(
    token: string | undefined,
    loanGrantorId: number
  ): Promise<LoanGrantor | null> {
    try {
      const response = await this.service.get<LoanGrantor>({
        endpoint: createUrl({
          url: `/lease/loan-grantor/${loanGrantorId}`,
        }),
        token,
      });
      return response;
    } catch (error) {
      console.error('Error al obtener los detalles del LoanGrantor:', error);
      return null;
    }
  }

  public async getCompanyDetails(
    token: string | undefined,
    companyId: number
  ): Promise<Company | null> {
    try {
      const response = await this.service.get<Company>({
        endpoint: createUrl({
          url: `/lease/loan-grantor/company/${companyId}`,
        }),
        token,
      });
      return response;
    } catch (error) {
      console.error('Error al obtener los detalles de la compañia:', error);
      return null;
    }
  }

  public async create({
    token,
    data,
    product,
    selectedLeaseOption,
    worksInACompany,
    country_user_id,
  }: {
    token: string | undefined;
    data: CreateLeaseFormData;
    product: ProductModel;
    selectedLeaseOption: string;
    worksInACompany: boolean;
    country_user_id: string;
  }) {
    const selectedLeaseInfo = product.leaseOptions[selectedLeaseOption];

    const randomString = Math.random().toString().slice(2);

    const payload: CreateLeaseBackendBody = {
      company: {
        name: data.companyName + randomString,
        webPage: data.companyWebpage,
        instagram: '@company',
        facebook: '@company',
        address: 'Company address',
      },
      loanGrantor: {
        firstName: data.companyBossFirstName,
        lastName: data.companyBossLastName,
        relationship: 'Boss',
        phoneNumber: data.companyBossPhone,
        email: !worksInACompany
          ? `${data.companyBossEmail}@email.com`
          : data.companyBossEmail,
        addressRoom: 'Loan grantor address room',
        landLineNumber: data.companyBossPhone,
      },
      lease: {
        country_user_id: country_user_id,
        type: 'lease',
        state: 'Lease state',
        userScore: 1,
        product: product.id,
        fullProductPrice: product.cashPrice,
        initialFee: selectedLeaseInfo.initialFee,
        monthlyFee: Math.round(selectedLeaseInfo.weeklyFee),
        feesNumber: Number.parseInt(selectedLeaseOption, 10),
        address: 'Lease address', // TODO: Change this and make it dynamic
        leaseReason: 'Lease reason',
      },
    };

    return this.service.post<CreateLeaseBackendResponse>({
      endpoint: createUrl({
        url: '/lease/allcountryid',
      }),
      data: payload,
      token,
    });
  }

  public async createCash({
    countryUserId,
    token,
    product,
    paymentType,
  }: {
    token: string | undefined;
    product: ProductModel;
    paymentType: string;
    countryUserId: string;
  }) {
    const payload: CreateLeaseBackendBody = {
      company: {
        name: Math.random().toString().slice(2),
        webPage: Math.random().toString().slice(2),
        instagram: '@company',
        facebook: '@company',
        address: 'Company address',
      },
      loanGrantor: {
        firstName: Math.random().toString().slice(2),
        lastName: Math.random().toString().slice(2),
        relationship: Math.random().toString().slice(2),
        phoneNumber: Math.random().toString().slice(2),
        email: `${Math.random().toString().slice(2)}@email.com`,
        addressRoom: Math.random().toString().slice(2),
        landLineNumber: Math.random().toString().slice(2),
      },
      lease: {
        country_user_id: countryUserId,
        type: 'purchase',
        state: 'Lease state',
        userScore: 1,
        product: product.id,
        fullProductPrice: product.cashPrice,
        initialFee: 1,
        monthlyFee: 1,
        feesNumber: 1,
        address: 'Caracas', // TODO: Change this and make it dynamic
        leaseReason: 'Lease reason',
      },
    };

    const res = await this.service.post<CreateLeaseBackendResponse>({
      endpoint: createUrl({
        url: '/lease/allcountryid',
      }),
      data: payload,
      token,
    });

    return this.service.post<CreateLeasePaymentBackendResponse>({
      endpoint: createUrl({
        url: `/payment/lease/${res.data}`,
      }),
      data: {
        amount: product.cashPrice,
        currency: 'USD',
        exchange_rate: 1,
        type: paymentType,
        date: new Date().toISOString(),
      },
      token,
    });
  }
}
