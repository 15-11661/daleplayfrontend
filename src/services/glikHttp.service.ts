import axios from 'axios';

export type DataContentServiceType = {
  params?: {
    [key: string]: string | number;
  };
  data?: unknown;
  token?: string;
  customHeaders?: object | undefined;
  endpoint: string;
};

export class GlikHttpService {
  private url: string;

  constructor() {
    this.url = process.env.NEXT_PUBLIC_API_URL
      ? `${process.env.NEXT_PUBLIC_API_URL}`
      : 'http://localhost:8000/api';
  }

  public async get<T>(
    properties: Omit<DataContentServiceType, 'data'>
  ): Promise<T> {
    return this._httpRequest('GET', properties);
  }

  public async post<T>(properties: DataContentServiceType): Promise<T> {
    return this._httpRequest<T>('POST', properties);
  }

  public async put<T>(properties: DataContentServiceType): Promise<T> {
    return this._httpRequest('PUT', properties);
  }

  public async delete<T>(properties: DataContentServiceType): Promise<T> {
    return this._httpRequest('DELETE', properties);
  }
  public async patch<T>(properties: DataContentServiceType): Promise<T> {
    return this._httpRequest<T>('PATCH', properties);
  }

  async _httpRequest<T>(
    method: 'POST' | 'GET' | 'DELETE' | 'PUT' | 'PATCH',
    properties: DataContentServiceType
  ): Promise<T> {
    const { token, ...restProperties } = properties;

    const authorizationHeader = token
      ? {
          Authorization: `Token ${token}`,
        }
      : {};

    const customHeaders = properties?.customHeaders || {};

    // Necesario para el endpoints con header de Content-Type distinto a application/json
    if (customHeaders.hasOwnProperty('Content-Type')) {
      return axios<T>({
        ...restProperties,
        method,
        url: `${this.url}${properties.endpoint}`,
        headers: {
          'Content-Type': 'application/json',
          ...properties?.customHeaders,
          ...authorizationHeader,
        },
      }).then((response) => {
        return response.data as T;
      });
    }

    // Axios instance
    return axios<T>({
      ...restProperties,
      method,
      url: `${this.url}${properties.endpoint}`,
      headers: {
        ...this.headers,
        ...properties?.customHeaders,
        ...authorizationHeader,
      },
    }).then((response) => {
      return response.data as T;
    });
  }

  get headers(): object {
    return {
      'Content-Type': 'application/json',
    };
  }
}