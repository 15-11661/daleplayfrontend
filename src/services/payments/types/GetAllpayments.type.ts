export interface Payment {
  id: number;
  amount: number;
  currency: string;
  exchangeRate: number;
  date: string;
  type: string;
  createdAt: string;
  user: string;
  lease: string;
  status: string;
  initial: boolean;
}

export interface GetPaymentsBackendResponseType {
  message: string;
  data: BackendPaymentPagination;
}

export interface BackendPaymentPagination {
  currentPage: number;
  pageSize: number;
  totalElements: number;
  totalPages: number;
  results: Payment[];
}

export interface PaymentStatusChangeResponse {
  message: string;
  new_status: string;
}

export interface PaymentStatusChangeSuccessfully {
  message: string;
  data: string;
}
