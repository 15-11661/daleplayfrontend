import { GlikHttpService } from '@/services/glikHttp.service';
import { Payment } from './types/GetAllpayments.type';
import { promise } from 'zod';

export class PaymentsService {
  private service: GlikHttpService;

  constructor() {
    this.service = new GlikHttpService();
  }

  public async getPaymentsByLeaseId(
    leaseId: number,
    token: string | undefined
  ): Promise<Payment[]> {
    const response = await this.service.get<{ data: Payment[] }>({
      endpoint: `/payment/getalllease/${leaseId}`,
      token,
    });
    console.log('Response from getPaymentsByLeaseId:', response); // Log the response

    return response.data;
  }
}