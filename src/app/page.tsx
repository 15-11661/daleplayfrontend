import React from 'react';
import Image from 'next/image';
import DalePlayLogo from '@/../public/DalePlay-logo.png';
import type { Metadata } from 'next';
import { LoginForm } from '@/components/forms/loginForm.component';
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from '@/components/ui/card.component';

export const metadata: Metadata = {
  title: 'DALE PLAY Admin',
};

export default function LoginPage() {
  return (
    <section className="container w-full min-h-screen flex items-center justify-center flex-col space-y-6">
      <div className="flex bg-primary items-center justify-center">
      <Image src={DalePlayLogo} alt="DalePlay Logo" width={350} height={200} />
      </div>
      <Card>
        <CardHeader className="relative flex items-center justify-center px-5">
          <div className="flex w-full items-center justify-center">
            <CardTitle className="text-xl">Bienvenido</CardTitle>
          </div>
          <CardDescription className="flex bg-white text-black text-center">
          Inicia seccion en tu cuenta de DALE PLAY
          </CardDescription>
        </CardHeader>
        <CardContent>
          <LoginForm />
        </CardContent>
      </Card>
    </section>
  );
}