import { CreateOrder } from '@/components/orders/create.component';

export default function NewOrderPage() {
  return <CreateOrder />;
}
