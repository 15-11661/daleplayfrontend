'use client';
import { useEffect, useCallback } from 'react';
import { Title } from '@/components/typography/title.component';
import { ProductsService } from '@/services/products/products.service';
import { LeaseModel } from '@/models/lease.model';
import { useToken } from '@/stores/auth/auth.store';
import type { Pagination } from '@/models/pagination.model';
import { useState } from 'react';
import { exportToXLSX } from './exportToXLSX';
import { Button } from '@/components/ui/button.component';
import Link from 'next/link';
import { formatDate } from '@/lib/utils';
import { UserModel } from '@/models/user.model';
import { useAuthService } from '@/stores/auth/auth.store';
import { useQuery } from '@tanstack/react-query';
import { OrdersList } from './orders.component';
import { useDebounce } from 'use-debounce';

export type PageInfo = { page: string; pageSize: string };

export default function OrdersPage() {
  const [leases, setLeases] = useState<LeaseModel[]>([]);
  const [users, setUsers] = useState<{ [userId: string]: UserModel }>({});
  const token = useToken() || '';
  const [selectedOptions, setSelectedOptions] = useState<{
    [key: number]: string;
  }>({});
  const authService = useAuthService();
  const productsService = new ProductsService();
  const [searchTerm, setSearchTerm] = useState('');
  const [pageInfo, setPageInfo] = useState({ page: '1', pageSize: '10' });
  const [debouncedSearchTerm] = useDebounce(searchTerm, 100);

  const handleOnSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const leasesQuery = useQuery({
    queryKey: ['products', token, pageInfo, debouncedSearchTerm],
    queryFn: async () =>
      productsService.getAllLeases(token, {
        ...pageInfo,
        search: debouncedSearchTerm,
      }),
    onSuccess: (data: Pagination<LeaseModel>) => {
      if (Array.isArray(data.results)) {
        const initialSelectedOptions = data.results.reduce(
          (options, lease) => {
            options[lease.id] = lease.status;
            return options;
          },
          {} as { [key: number]: string }
        );
        setSelectedOptions(initialSelectedOptions);
        setLeases(data.results);
      }
    },
  });

  const disabledPrevButton = Number.parseInt(pageInfo.page) === 1;
  const disabledNextButton =
    leasesQuery.data &&
    Number.parseInt(pageInfo.page) === leasesQuery.data.totalPages;

  const handleOnPreviousPage = () => {
    if (disabledPrevButton) return;

    setPageInfo((oldValue) => ({
      ...oldValue,
      page: `${Number.parseInt(oldValue.page) - 1}`,
    }));
  };

  const handleOnNextPage = () => {
    if (disabledNextButton) return;

    setPageInfo((oldValue) => ({
      ...oldValue,
      page: `${Number.parseInt(oldValue.page) + 1}`,
    }));
  };

  const fetchUserById = useCallback(
    async (userId: string) => {
      const user = await authService.getUserById(userId, token);
      return user;
    },
    [token, authService]
  );
  useEffect(() => {
    // Obtener información del usuario para cada arrendamiento
    async function fetchUserDetails() {
      const userMap: { [userId: string]: UserModel } = {};
      for (const lease of leases) {
        if (!users[lease.user]) {
          const user = await fetchUserById(lease.user);
          if (user) {
            userMap[lease.user] = user;
          }
        }
      }
      setUsers((prevUsers) => ({ ...prevUsers, ...userMap }));
    }

    // Llamar a fetchUserDetails solo cuando cambien las dependencias relevantes
    if (leases.length > 0 && Object.keys(users).length === 0) {
      fetchUserDetails();
    }
  }, [leases, users, fetchUserById]);

  const handleOptionChange = (leaseId: number, newStatus: string) => {
    const productService = new ProductsService();
    productService
      .updateLeaseStatus(leaseId, newStatus, token)
      .then((response) => {
        // Actualiza las opciones seleccionadas en el estado local
        setSelectedOptions((prevOptions) => ({
          ...prevOptions,
          [leaseId]: newStatus,
        }));
      })
      .catch((error) => {
        console.error(
          'Error al actualizar el estado del arrendamiento:',
          error
        );
      });
  };

  return (
    <>
      <div>
        <h1 className="text-2xl font-semibold mb-2 text-primary font-serif">
          Ordenes
        </h1>
      </div>
      <div className="flex justify-end mb-2">
      <section className="flex justify-between">
        <Link href="/dashboard/orders/new">
          <Button>Crear Orden</Button>
        </Link>
      </section>
        <input
          type="text"
          value={debouncedSearchTerm}
          onChange={handleOnSearch}
          placeholder="Buscar orden..."
          className="px-3 py-2 border rounded-md"
        />
      </div>
      {leasesQuery.isLoading && <Title>Cargando ordenes...</Title>}
      {leasesQuery.data && (
        <OrdersList
          paginatedOrders={leasesQuery.data}
          users={users}
          selectedOptions={selectedOptions}
          handleOptionChange={handleOptionChange}
        />
      )}
      <section className="mx-auto flex justify-center space-x-4 mt-16">
        <Button disabled={disabledPrevButton} onClick={handleOnPreviousPage}>
          Anterior
        </Button>
        <Button disabled={disabledNextButton} onClick={handleOnNextPage}>
          Siguiente
        </Button>
      </section>
    </>
  );
}
