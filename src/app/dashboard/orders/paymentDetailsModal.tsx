import React, { useEffect, useState } from 'react';
import { PaymentsService } from '@/services/payments/payments.service';
import { LeasesService } from '@/services/leases/leases.service';
import { LoanGrantor } from '../../../services/leases/types/GetLeaseByIdBackendResponse.types';
import { Company } from '../../../services/leases/types/GetLeaseByIdBackendResponse.types';
import { useToken } from '@/stores/auth/auth.store';
import { useAuthService } from '@/stores/auth/auth.store';
import { UserModel } from '@/models/user.model';
import { Payment } from '@/services/payments/types/GetAllpayments.type';
import { Button } from '@/components/ui/button.component';
import { formatDate } from '@/lib/utils';
import { LeaseModel } from '@/models/lease.model';
import '@/components/componentStyle.css';

interface PaymentDetailsModalProps {
  leaseId: number;
  userId: string;
  onClose: () => void;
}

const paymentsService = new PaymentsService();
const leasesService = new LeasesService();

export default function PaymentDetailsModal({ leaseId, userId, onClose }: PaymentDetailsModalProps) {
  const [pastPayments, setPastPayments] = useState<Payment[]>([]);
  const [nextPayments, setNextPayments] = useState<Payment[]>([]);
  const [loading, setLoading] = useState(true);
  const [leaseDetails, setLeaseDetails] = useState<LeaseModel | null>(null);
  const [userDetails, setUserDetails] = useState<UserModel | null>(null);

  const token = useToken() || '';
  const authService = useAuthService();

  useEffect(() => {
    const fetchDetails = async () => {
      try {
        // Obtener detalles del arrendamiento
        const lease = await leasesService.getByUserAndId(userId, leaseId, token);
        setLeaseDetails(lease);

        // Obtener detalles del usuario
        const user = await authService.getUserById(userId, token);
        setUserDetails(user || null);

        // Obtener pagos
        const payments = await paymentsService.getPaymentsByLeaseId(leaseId, token);
        setPastPayments(payments.filter(payment => payment.status === 'PAID'));
        setNextPayments(payments.filter(payment => payment.status === 'PENDING'));

      } catch (error) {
        console.error('Error fetching details:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchDetails();
  }, [leaseId, userId, token, authService]);

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
      <div className="modal-container bg-white w-11/12 md:max-w-3xl mx-auto rounded shadow-lg z-50">
        <div className="modal-content py-5 text-left px-10 overflow-y-auto">
          <div className="flex justify-between items-center pb-3">
            <h2 className="text-2xl font-bold green-text">Detalles del Arrendamiento</h2>
            <button className="modal-close" onClick={onClose}>&times;</button>
          </div>
          {loading ? (
            <p>Cargando...</p>
          ) : (
            <div>
              {userDetails && (
                <>
                  <h3 className="my-4 green-text font-bold text-lg">Usuario</h3>
                  <table className="w-full">
                    <tbody>
                      <tr>
                        <td className="font-bold py-1">Username</td>
                        <td>{userDetails.username}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Cedula</td>
                        <td>{userDetails.countryUserId}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Nombres del usuario</td>
                        <td>{userDetails.firstName}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Apellidos del usuario</td>
                        <td>{userDetails.lastName}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Telefono</td>
                        <td>{userDetails.phoneNumber}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Correo electrónico</td>
                        <td>{userDetails.email}</td>
                      </tr>
                    </tbody>
                  </table>
                </>
              )}
              <h3 className="my-4 green-text font-bold text-lg">Próximos Pagos</h3>
              { leaseDetails ? (
                <p className="font-bold py-1 mb-4">{leaseDetails.nextPaymentDate ? formatDate(leaseDetails.nextPaymentDate) : 'No hay información del próximo pago disponible.'}</p>
              ) : (
                <p className="font-bold py-1 mb-4">No hay información del próximo pago disponible.</p>
              )}
              <h3 className="my-4 green-text font-bold text-lg">Pagos Hechos</h3>
              {pastPayments.length > 0 ? (
                <table className="w-full">
                  <thead>
                    <tr>
                      <th className="font-bold py-1">Fecha de Pago</th>
                      <th className="font-bold py-1">Monto</th>
                      <th className="font-bold py-1">Moneda</th>
                      <th className="font-bold py-1">Inicial</th>
                    </tr>
                  </thead>
                  <tbody>
                    {pastPayments.map((payment, index) => (
                      <tr key={index}>
                        <td>{formatDate(payment.date)}</td>
                        <td>{payment.amount}</td>
                        <td>{payment.currency}</td>
                        <td>{payment.initial ? 'Sí' : 'No'}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              ) : (
                <p className="font-bold py-1 mb-4">No hay pagos pasados disponibles.</p>
              )}
              <div className="flex justify-end mt-4">
                <Button onClick={onClose}>Cerrar</Button>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}