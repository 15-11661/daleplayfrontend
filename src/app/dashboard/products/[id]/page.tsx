import { ProductDetail } from '@/components/product/productDetail.component';

export default function Page({ params }: { params: { id: string } }) {
  return <ProductDetail productId={params.id} />;
}
