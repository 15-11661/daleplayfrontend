import { CreateProduct } from '@/components/product/create.component';

export default function NewProductPage() {
  return <CreateProduct />;
}
