import { PaginatedProductsList } from '@/components/product/paginatedProductsList.component';
import { Title } from '@/components/typography/title.component';
import { Button } from '@/components/ui/button.component';
import Link from 'next/link';

export default function ProductsPage() {
  return (
    <>
      <section className="flex justify-between">
        <Title>Productos</Title>
        <Link href="/dashboard/products/new">
          <Button>Crear Producto</Button>
        </Link>
      </section>

      <PaginatedProductsList />
    </>
  );
}
