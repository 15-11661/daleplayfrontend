import Image from 'next/image';
import type { Metadata } from 'next';
import { SidebarAvatar } from '@/components/navigation/sidebar/avatar.component';
import { SidebarNav } from '@/components/navigation/sidebar/sidebarNav.component';
import DalePlayLogo from '@/../public/DalePlay-logo.png';
import { RoutesProtectionProvider } from '@/components/routesProtectionProvider/routesProtectionProvider.component';

export const metadata: Metadata = {
  title: 'Dale Play',
  description: 'Tu moto en cuotas',
};

const items = [
  {
    title: 'Inicio',
    href: '/dashboard',
  },
  {
    title: 'Usuarios',
    href: '/dashboard/users',
  },
  {
    title: 'Productos',
    href: '/dashboard/products',
  },
  {
    title: 'Categorías',
    href: '/dashboard/categories',
  },
  {
    title: 'Ordenes',
    href: '/dashboard/orders',
  },
  {
    title: 'Pagos',
    href: '/dashboard/payments',
  },
];

export default function DashboardLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <RoutesProtectionProvider>
      <div className="min-h-screen">
        <aside className="fixed w-[320px] bg-secondary min-h-screen z-10 flex flex-col">
          <div className="relative w-full bg-primary flex items-center justify-center px-5 h-16">
            <Image
              src={DalePlayLogo}
              alt="DalePlay"
              className="object-contain w-150 logo-overlap-override"
            />
          </div>

          <SidebarNav items={items} />

          <SidebarAvatar />
        </aside>

        <section className="ml-[320px]">
          <nav className="h-16 w-full bg-primary mb-4 px-4 flex items-center" />
          <div className="p-4 lg:p-12">{children}</div>
        </section>
      </div>
    </RoutesProtectionProvider>
  );
}