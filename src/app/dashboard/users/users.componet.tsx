'use client';
import { Pagination } from '@/models/pagination.model';
import { UserModel } from '@/models/user.model';
import { AuthService } from '@/services/auth/auth.service';
import { formatDate } from '@/lib/utils';
import { useState } from 'react';
import { useEffect } from 'react';
import UserDetailsModal from '@/app/dashboard/users/userDetailsModal';
import { set } from 'zod';


interface UserListProps {
  paginatedUsers: Pagination<UserModel>;
}

export function UserList({ paginatedUsers }: UserListProps) {
  const { results: users } = paginatedUsers;
  const [selectedUserId, setSelectedUserId] = useState<number | null>(null);

  const openLeaseDetailsModal = (leaseId: number) => {
    console.log(leaseId);
    setSelectedUserId(leaseId);
  };

  const closeLeaseDetailsModal = () => {
    setSelectedUserId(null);
  };

  return (
    <div className="py-3">
      <div>
        <div className="bg-white rounded-lg shadow overflow-hidden">
          <table className="min-w-full">
            <thead className="bg-primary text-white text-center text-xs text-leading-4 font-medium  uppercase tracking-wider">
              <tr>
                <th className="px-6 py-3 ">Cedula</th>
                <th className="px-6 py-3 ">User Name</th>
                <th className="px-6 py-3 ">First Name</th>
                <th className="px-6 py-3 ">Last Name</th>
                <th className="px-6 py-3 ">Email</th>
                <th className="px-6 py-3 ">Phone Number</th>
                <th className="px-6 py-3 ">Fecha de creación</th>
              </tr>
            </thead>
            <tbody className="divide-y divide-gray-200">
              {users ? (
                users.map((user, index) => (
                  <tr
                    key={user.id}
                    className={index % 2 === 0 ? 'bg-gray-200' : 'bg-white'}
                  >
                    <td className="p-2 text-green-500 hover:underline">
                      <button
                        onClick={() =>
                          openLeaseDetailsModal(user.id)
                        }
                      >
                        {user.countryUserId}
                      </button>
                    </td>
                    <td className="px-6 py-4 whitespace-no-wrap text-center ">
                      {user.username}
                    </td>
                    <td className="px-6 py-4 whitespace-no-wrap">
                      {user.firstName}
                    </td>
                    <td className="px-6 py-4 whitespace-no-wrap">
                      {user.lastName}
                    </td>
                    <td className="px-6 py-4 whitespace-no-wrap">
                      {user.email}
                    </td>
                    <td className="px-4 py-4 whitespace-no-wrap">
                      {user.phoneNumber}
                    </td>
                    <td className="px-6 py-4 whitespace-no-wrap">
                      {formatDate(user.dateJoined)}
                    </td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td className="px-6 py-4 whitespace-no-wrap" colSpan={5}>
                    Cargando usuarios...
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <div>
          {selectedUserId !== null && (
            <UserDetailsModal
              userId={selectedUserId}
              onClose={closeLeaseDetailsModal}
            />
          )}
        </div>
      </div>
    </div>
  );
}