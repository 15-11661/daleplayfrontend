'use client';
import { Title } from '@/components/typography/title.component';
import { useAuthService, useToken } from '@/stores/auth/auth.store';
import { useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useDebounce } from 'use-debounce';
import { Button } from '@/components/ui/button.component';
import { UserList } from './users.componet';
import Link from 'next/link';

export type PageInfo = { page: string; pageSize: string };

export default function UsersPage() {
  const authService = useAuthService();
  const token = useToken() || '';
  const [searchTerm, setSearchTerm] = useState('');
  const [pageInfo, setPageInfo] = useState({ page: '1', pageSize: '10' });
  const [debouncedSearchTerm] = useDebounce(searchTerm, 100);

  const usersQuery = useQuery({
    queryKey: ['user', token, pageInfo, searchTerm],
    queryFn: async () => {
      if (debouncedSearchTerm) {
        const user = await authService.getUserById(searchTerm, token);

        return {
          currentPage: 1,
          pageSize: 1,
          results: user ? [user] : [],
          totalElements: user ? 1 : 0,
          totalPages: 1,
        };
      } else {
        return authService.getUsers(token, { ...pageInfo });
      }
    },
  });
  const disabledPrevButton = Number.parseInt(pageInfo.page) === 1;
  const disabledNextButton =
    usersQuery.data &&
    Number.parseInt(pageInfo.page) === usersQuery.data.totalPages;

  const handleOnPreviousPage = () => {
    if (disabledPrevButton) return;
    setPageInfo((oldValue) => ({
      ...oldValue,
      page: `${Number.parseInt(oldValue.page) - 1}`,
    }));
  };

  const handleOnNextPage = () => {
    if (disabledNextButton) return;
    setPageInfo((oldValue) => ({
      ...oldValue,
      page: `${Number.parseInt(oldValue.page) + 1}`,
    }));
  };

  const handleSearchTermChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const inputText = event.target.value;
    const numericValue = inputText.replace(/[^0-9]/g, '');

    setSearchTerm(numericValue);
  };

  return (
    <>
      <div>
        <h1 className="text-2xl font-semibold mb-2 text-primary font-serif">
          Usuarios
        </h1>
      </div>
      <div className="flex justify-end mb-2 space-x-10">
        <Link href="/dashboard/users/new">
          <Button>Crear usuario</Button>
        </Link>
        <input
          type="text"
          placeholder="Buscar usuario..."
          value={debouncedSearchTerm}
          onChange={handleSearchTermChange}
          className="px-3 py-2 border rounded-md"
        />
      </div>
      {usersQuery.isLoading && <Title>Cargando usuarios...</Title>}

      {usersQuery.data && <UserList paginatedUsers={usersQuery.data} />}

      <section className="mx-auto flex justify-center space-x-4 mt-16">
        <Button disabled={disabledPrevButton} onClick={handleOnPreviousPage}>
          Anterior
        </Button>
        <Button disabled={disabledNextButton} onClick={handleOnNextPage}>
          Siguiente
        </Button>
      </section>
    </>
  );
}
