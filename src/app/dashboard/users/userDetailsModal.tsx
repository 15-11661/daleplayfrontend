import '@/components/componentStyle.css';
import React, { useEffect, useState } from 'react';
import { LeasesService } from '@/services/leases/leases.service';
import { useToken } from '@/stores/auth/auth.store';
import { useQuery } from '@tanstack/react-query';
import '@/components/componentStyle.css';
import { formatDate } from '@/lib/utils';
import { CloseIcon } from '@/app/icons/close';
import { UserModel } from '@/models/user.model';

interface UserDetailsModalProps {
  userId: string | number;
  onClose: () => void;
}

function UserDetailsModal({ userId, onClose }: UserDetailsModalProps) {
  const token = useToken() || '';
  const leaseService = new LeasesService();
  const [userDetails, setUserDetails] = useState<UserModel | null>(null);

  const leasesQuery = useQuery({
    queryKey: ['leases', token],
    queryFn: async () => {
      return await leaseService.getAllLeasesByUser(userId.toString(), token);
    },
  });

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
      <div className="modal-container bg-white w-11/12 md:max-w-4xl mx-auto rounded shadow-lg z-50">
        <div className="modal-content py-5 text-left px-10 overflow-y-auto max-h-screen">
          <div className="flex justify-between items-center pb-3">
            <p className="text-2xl font-bold green-text">Detalle del Usuario</p>
            <button className="modal-close" onClick={onClose}>
              <CloseIcon />
            </button>
          </div>
          {leasesQuery.isSuccess ? (
            <div>
              <div className="flex flex-col">
                <p className="text-lg font-bold">ID del usuario:</p>
                <p>{userId}</p>
                <div className="flex flex-col">
                  <p className="text-lg font-bold">Arrendamientos:</p>
                  <table className="table-auto w-full">
                    <thead>
                      <tr>
                        <th className="px-6 py-1 text-center whitespace-nowrap">ID</th>
                        <th className="px-6 py-1 text-center whitespace-nowrap">Estado</th>
                        <th className="px-6 py-1 text-center whitespace-nowrap">Precio de cuota</th>
                        <th className="px-6 py-1 text-center whitespace-nowrap">Fecha inicial</th>
                        <th className="px-6 py-1 text-center whitespace-nowrap">Producto</th>
                        <th className="px-6 py-1 text-center whitespace-nowrap">Fecha de siguiente pago</th>
                        <th className="px-6 py-1 text-center whitespace-nowrap">Pago inicial</th>
                      </tr>
                    </thead>
                    <tbody>
                      {leasesQuery.data.map((lease) => (
                        <tr key={lease.id}>
                          <td className="font-bold py-1 px-1 text-center">{lease.id}</td>
                          <td className="font-bold py-1 px-1 text-center">{lease.status}</td>
                          <td className="font-bold py-1 px-1 text-center">{lease.monthlyFee}</td>
                          <td className="font-bold py-1 px-1 text-center">{formatDate(lease.createdAt)}</td>
                          <td className="font-bold py-1 px-1 text-center">{lease.product}</td>
                          <td className="font-bold py-1 px-1 text-center">{formatDate(lease.nextPaymentDate)}</td>
                          <td className="font-bold py-1 px-1 text-center">{lease.initialFee}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          ) : (
            <p className="text-lg">Cargando detalles del arrendamiento...</p>
          )}
        </div>
      </div>
    </div>
  );
}

export default UserDetailsModal;