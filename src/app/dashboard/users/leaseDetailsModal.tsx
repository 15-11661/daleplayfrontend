import '@/components/componentStyle.css'
import React, { useEffect, useState } from 'react';
import { LeasesService } from '@/services/leases/leases.service';
import { useToken } from '@/stores/auth/auth.store';
import { useAuthService } from '@/stores/auth/auth.store';
import { UserModel } from '@/models/user.model';
import { CloseIcon } from '@/app/icons/close';
import { LoanGrantor } from '../../../services/leases/types/GetLeaseByIdBackendResponse.types';
import { Company } from '../../../services/leases/types/GetLeaseByIdBackendResponse.types';
import { LeaseModel } from '@/models/lease.model';
import '@/components/componentStyle.css';

interface LeaseDetailsModalProps {
  leaseId: number;
  userId: string;
  onClose: () => void;
}

function LeaseDetailsModal({
  leaseId,
  userId,
  onClose,
}: LeaseDetailsModalProps) {
  const [leaseDetails, setLeaseDetails] = useState<LeaseModel | null>(null);
  const token = useToken() || '';
  const authService = useAuthService();
  const [userDetails, setUserDetails] = useState<UserModel | null>(null);
  const [loanGrantorDetails, setLoanGrantorDetails] =
    useState<LoanGrantor | null>(null);
  const [companyDetails, setCompanyDetails] = useState<Company | null>(null);

  useEffect(() => {
    async function fetchLeaseDetails() {
      const leasesService = new LeasesService();
      try {
        const [lease, loanGrantorDetails, userDetails] = await Promise.all([
          leasesService.getByUserAndId(userId, leaseId, token),
          (async () => {
            const leaseResult = await leasesService.getByUserAndId(
              userId,
              leaseId,
              token
            );
            return leaseResult?.loanGrantor
              ? leasesService.getLoanGrantorDetails(
                  token,
                  leaseResult.loanGrantor
                )
              : null;
          })(),
          authService.getUserById(userId, token),
        ]);

        setLeaseDetails(lease);
        if (lease != null) {
          const loanGrantorId = lease.loanGrantor;
          const loanGrantorDetails = await leasesService.getLoanGrantorDetails(
            token,
            loanGrantorId
          );
          setLoanGrantorDetails(loanGrantorDetails);
        }
        if (loanGrantorDetails) {
          const companyId = loanGrantorDetails?.company;
          if (companyId !== undefined) {
            const companyDetails = await leasesService.getCompanyDetails(
              token,
              companyId
            );
            setCompanyDetails(companyDetails);
          }
        }

        if (userDetails) {
          setUserDetails(userDetails);
        }
      } catch (error) {
        console.error(
          'Error al obtener los detalles del arrendamiento:',
          error
        );
      }
    }

    fetchLeaseDetails();
  }, [userId, leaseId, token, authService]);

  return (
    <div className="fixed inset-0 flex items-center justify-center z-50">
      <div className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
      <div className="modal-container bg-white w-11/12 md:max-w-3xl mx-auto rounded shadow-lg z-50">
        <div className="modal-content py-4 text-left px-6 overflow-y-auto">
          <div className="flex justify-between items-center pb-3">
            <p className="text-2xl font-bold green-text">
              Detalles del Arrendamiento {leaseDetails?.id}
            </p>
            <button
              type="button"
              className="modal-close"
              onClick={onClose}
              title="Close Modal"
            >
              <CloseIcon />
            </button>
          </div>
          {leaseDetails ? (
            <div>
              <table className="w-full">
                <tbody>
                  <p className="my-4 green-text font-bold text-lg">
                    {' '}
                    Usuario
                  </p>
                  {userDetails ? (
                    <>
                      <tr>
                        <td className="font-bold py-1 ">Username</td>
                        <td>{userDetails.username}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Nombres del usuario</td>
                        <td>{userDetails.firstName}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">
                          Apellidos del usuario
                        </td>
                        <td>{userDetails.lastName}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">
                          Telefono celular del usuario
                        </td>
                        <td>{userDetails.phoneNumber}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Correo electrónico</td>
                        <td>{userDetails.email}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Dirección</td>
                        <td>{userDetails.birthplace}</td>
                      </tr>
                    </>
                  ) : (
                    <tr>
                      <td>Cargando detalles del usuario...</td>
                    </tr>
                  )}
                  <p className="my-4 green-text font-bold text-lg">
                    {' '}
                    Empleo
                  </p>
                  {companyDetails ? (
                    <>
                      <tr>
                        <td className="font-bold py-1">Compañía</td>
                        <td>{companyDetails.name}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Pagina web</td>
                        <td>{companyDetails.webPage}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">Dirección</td>
                        <td>{companyDetails.address}</td>
                      </tr>
                    </>
                  ) : (
                    <tr>
                      <td>Cargando detalles de la compañía...</td>
                    </tr>
                  )}
                  <p className="my-4 green-text font-bold text-lg">
                    {' '}
                    Acreedor
                  </p>
                  {loanGrantorDetails ? (
                    <>
                      <tr>
                        <td className="font-bold py-1">
                          Nombres del jefe inmediato
                        </td>
                        <td>{loanGrantorDetails.firstName}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">
                          Apellidos del jefe inmediato
                        </td>
                        <td>{loanGrantorDetails.lastName}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">
                          Correo electrónico del jefe inmediato
                        </td>
                        <td>{loanGrantorDetails.email}</td>
                      </tr>
                      <tr>
                        <td className="font-bold py-1">
                          Telefono del jefe inmediato
                        </td>
                        <td>{loanGrantorDetails.landLineNumber}</td>
                      </tr>
                    </>
                  ) : (
                    <tr>
                      <td>Cargando detalles del LoanGrantor...</td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          ) : (
            <p className="text-lg">Cargando detalles del arrendamiento...</p>
          )}
        </div>
      </div>
    </div>
  );
}

export default LeaseDetailsModal;
