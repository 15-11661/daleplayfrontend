'use client';
import React, { useState } from 'react';
import { Categories } from '@/components/category/categories.component';
import { Title } from '@/components/typography/title.component';
import { Button } from '@/components/ui/button.component';
import { CategoryService } from '@/services/categories/categories.service';
import { useToken, useAuthService } from '@/stores/auth/auth.store';

export default function CategoriesPage() {
  const categoryService = new CategoryService();
  const authService = useAuthService();
  const token = useToken() || '';
  const [categoryName, setCategoryName] = useState('');
  const [createdCategories, setCreatedCategories] = useState<string[]>([]);

  const handleCreateCategory = async () => {
    try {
      const currentUser = await authService.getCurrentUser(token);
      if (currentUser) {
        const userId = currentUser.id.toString();
        const categoryData = {
          id: userId,
          name: categoryName,
          parent: null, // Que valor hay que colocar?
        };

        await categoryService.createCategory(token, categoryData);

        setCreatedCategories([...createdCategories, categoryName]);

        setCategoryName('');

        window.location.reload();
      } else {
        console.error('No se pudo obtener el usuario actual.');
      }
    } catch (error) {
      console.error('Error al crear la categoría:', error);
    }
  };

  return (
    <>
      <section className="flex justify-between">
        <Title>Categorías</Title>
        <div className="flex">
          <input
            type="text"
            placeholder="Nombre de la categoría"
            value={categoryName}
            onChange={(e) => setCategoryName(e.target.value)}
          />
          <Button onClick={handleCreateCategory}>Crear Categoría</Button>
        </div>
      </section>
      <Categories />
    </>
  );
}
