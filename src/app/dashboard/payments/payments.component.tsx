'use client';
import { ProductsService } from '@/services/products/products.service';
import { Pagination } from '@/models/pagination.model';
import { Payment } from '@/services/payments/types/GetAllpayments.type';
import { Title } from '@/components/typography/title.component';
import { formatDate } from '@/lib/utils';
import { UserModel } from '@/models/user.model';

const productsService = new ProductsService();

interface PaymentsListProps {
  paginatedPayments: Pagination<Payment>;
  users: { [userId: string]: UserModel };
  selectedOptions: { [key: number]: string };
  handleOptionChange: (paymentId: number, newStatus: string) => void;
}

export function PaymentsList({
  paginatedPayments,
  users,
  selectedOptions,
  handleOptionChange,
}: PaymentsListProps) {
  const { results: payments } = paginatedPayments;

  return (
    <>
      <div>
        {payments.length === 0 ? (
          <p>No hay pagos que mostrar.</p>
        ) : (
          <>
            <div className="bg-white rounded-lg shadow overflow-hidden text-sm">
              <table className="min-w-full table-auto border-collapse border border-gray-200">
                <thead className="bg-primary text-white text-center text-xs  font-medium  uppercase tracking-wider">
                  <th className="p-2">ID</th>
                  <th>Estado</th>
                  <th>Monto</th>
                  <th>Usuario</th>
                  <th>
                    Fecha de <br className="xs:hidden" /> Creación
                  </th>
                  <th>Arrendamiento</th>
                  <th>Tipo</th>
                  <th className="p-2">Opciones</th>
                </thead>
                <tbody className="divide-y divide-gray-200 ">
                  {payments ? (
                    payments.map((payment, index) => (
                      <tr
                        key={payment.id}
                        className={
                          index % 2 === 0
                            ? 'bg-gray-200 text-center'
                            : 'bg-white text-center'
                        }
                      >
                        <td>{payment.id}</td>
                        <td>{payment.status}</td>
                        <td>{payment.amount}</td>
                        <td>
                          {users[payment.user]?.firstName}{' '}
                          {users[payment.user]?.lastName}
                        </td>
                        <td>{formatDate(payment.createdAt)}</td>
                        <td>{payment.lease}</td>
                        <td>{payment.type}</td>
                        <td>
                          <select
                            className={`py-2 border rounded-md shadow-sm focus:outline-none focus:ring focus:border-blue-300 text-center ${
                              index % 2 === 0 ? 'bg-gray-200' : 'bg-white'
                            }`}
                            value={selectedOptions[Number(payment.id)] || ''}
                            onChange={(e) => {
                              const newStatus = e.target.value;
                              handleOptionChange(Number(payment.id), newStatus);
                            }}
                          >
                            ¿<option value="PENDING">PENDING</option>
                            <option value="PAID">PAID</option>
                            <option value="CANCELED">CANCELED</option>
                          </select>
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr>
                      <td className="px-6 py-4 whitespace-no-wrap" colSpan={5}>
                        Cargando pagos...
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </>
        )}
      </div>
    </>
  );
}
