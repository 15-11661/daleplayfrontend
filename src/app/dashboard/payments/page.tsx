'use client';
import { Title } from '@/components/typography/title.component';
import { ProductsService } from '@/services/products/products.service';
import { useToken } from '@/stores/auth/auth.store';
import Link from 'next/link';
import { Button } from '@/components/ui/button.component';
import { Payment } from '@/services/payments/types/GetAllpayments.type';
import { useEffect, useState, useCallback } from 'react';
import type { Pagination } from '@/models/pagination.model';
import { useAuthActions, useAuthService } from '@/stores/auth/auth.store';
import { UserModel } from '@/models/user.model';
import { formatDate } from '@/lib/utils';
import { useQuery } from '@tanstack/react-query';
import { PaymentsList } from './payments.component';
import { useDebounce } from 'use-debounce';

export type PageInfo = { page: string; pageSize: string };

export default function PaymentsPage() {
  const token = useToken() || '';
  const [payments, setPayments] = useState<Payment[]>([]);
  const [selectedOptions, setSelectedOptions] = useState<{
    [key: number]: string;
  }>({});

  const authService = useAuthService();
  const [users, setUsers] = useState<{ [userId: string]: UserModel }>({});
  const productService = new ProductsService();
  const [pageInfo, setPageInfo] = useState({ page: '1', pageSize: '10' });
  const [searchTerm, setSearchTerm] = useState('');
  const [debouncedSearchTerm] = useDebounce(searchTerm, 100);

  const paymentsQuery = useQuery({
    queryKey: ['payments', token, pageInfo, debouncedSearchTerm],
    queryFn: async () =>
      productService.getAllPayments(token, {
        ...pageInfo,
        search: debouncedSearchTerm,
      }),
    onSuccess: (data: Pagination<Payment>) => {
      if (Array.isArray(data.results)) {
        const initialSelectedOptions = data.results.reduce(
          (options, payment) => {
            options[payment.id] = payment.status;
            return options;
          },
          {} as { [key: number]: string }
        );
        setSelectedOptions(initialSelectedOptions);
        setPayments(data.results);
      }
    },
  });

  const disabledPrevButton = Number.parseInt(pageInfo.page) === 1;
  const disabledNextButton =
    paymentsQuery.data &&
    Number.parseInt(pageInfo.page) === paymentsQuery.data.totalPages;

  const handleOnPreviousPage = () => {
    if (disabledPrevButton) return;

    setPageInfo((oldValue) => ({
      ...oldValue,
      page: `${Number.parseInt(oldValue.page) - 1}`,
    }));
  };

  const handleOnNextPage = () => {
    if (disabledNextButton) return;

    setPageInfo((oldValue) => ({
      ...oldValue,
      page: `${Number.parseInt(oldValue.page) + 1}`,
    }));
  };

  const fetchUserById = useCallback(
    async (userId: string) => {
      const user = await authService.getUserById(userId, token);
      return user;
    },
    [token, authService]
  );

  useEffect(() => {
    // Obtener información del usuario para cada arrendamiento
    async function fetchUserDetails() {
      const userMap: { [userId: string]: UserModel } = {};
      for (const lease of payments) {
        if (!users[lease.user]) {
          const user = await fetchUserById(lease.user);
          if (user) {
            userMap[lease.user] = user;
          }
        }
      }
      setUsers((prevUsers) => ({ ...prevUsers, ...userMap }));
    }

    // Llamar a fetchUserDetails solo cuando cambien las dependencias relevantes
    if (payments.length > 0 && Object.keys(users).length === 0) {
      fetchUserDetails();
    }
  }, [payments, users, fetchUserById]);

  const handleOptionChange = (paymentId: number, newStatus: string) => {
    const productService = new ProductsService();
    productService
      .updatePaymentStatus(paymentId, newStatus, token)
      .then((response) => {
        // Actualiza las opciones seleccionadas en el estado local
        setSelectedOptions((prevOptions) => ({
          ...prevOptions,
          [paymentId]: newStatus,
        }));
      })
      .catch((error) => {
        console.error(
          'Error al actualizar el estado del arrendamiento:',
          error
        );
      });
  };

  return (
    <>
      <h1 className="text-2xl font-semibold mb-2 text-primary font-serif">
        Pagos
      </h1>
      <div className="flex justify-end mb-2">
        <input
          type="text"
          value={debouncedSearchTerm}
          onChange={(event) => setSearchTerm(event.target.value)}
          placeholder="Buscar pago..."
          className="px-3 py-2 border rounded-md"
        />
      </div>

      {paymentsQuery.isLoading && <Title>Cargando Pagos...</Title>}
      {paymentsQuery.data && (
        <PaymentsList
          paginatedPayments={paymentsQuery.data}
          users={users}
          selectedOptions={selectedOptions}
          handleOptionChange={handleOptionChange}
        />
      )}
      <section className="mx-auto flex justify-center space-x-4 mt-16">
        <Button disabled={disabledPrevButton} onClick={handleOnPreviousPage}>
          Anterior
        </Button>
        <Button disabled={disabledNextButton} onClick={handleOnNextPage}>
          Siguiente
        </Button>
      </section>
    </>
  );
}
